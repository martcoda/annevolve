package Evolvers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import Model.IncomingEdge;
public class AFgaussian extends ActivationFunctionParent
{
	public AFgaussian()
	{}

	@Override
	public Double tryActivation(ArrayList<IncomingEdge> edgeList,
			Double biasWeight, Double biasValue) {
			
			Double summer = 0.0;
				
				for(IncomingEdge entry : edgeList)
				{
					summer += ( entry.inputValue * entry.weightValue);
				}
				
				summer += biasWeight * biasValue;
				
				Double result = Math.exp(-1*((summer*summer)/2));
			
		// TODO Auto-generated method stub
		return result;

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Gaussian";
	}
}
