package Evolvers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import Model.IncomingEdge;
public class AFhyperbolicTangent extends ActivationFunctionParent
{
	public AFhyperbolicTangent()
	{}

	@Override
	public Double tryActivation(ArrayList<IncomingEdge> edgeList,
			Double biasWeight, Double biasValue) {
			
			Double summer = 0.0;
				
				for(IncomingEdge entry : edgeList)
				{
					summer += ( entry.inputValue * entry.weightValue);
				}
				
				summer += biasWeight * biasValue;
				
				Double result = Math.tanh(Math.toRadians(summer));
				result = result * 100; // Because the results were too small. 
			
		return result;

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Hyperbolic tangent";
	}
}
