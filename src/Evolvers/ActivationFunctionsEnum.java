package Evolvers;

public enum ActivationFunctionsEnum {

	NULL,
	SIGMOID,
	HTAN,
	COSINE,
	GAUSSIAN;
	
}
