package Evolvers;

public enum FunctionsToApproximateEnum {

	LINEAR,
	CUBIC,
	SINE,
	TANH,
	XOR,
	COMPLEX
}
