package Evolvers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import Model.IncomingEdge;

public class AFnull extends ActivationFunctionParent
{
	public AFnull()
	{}
	
	public Double tryActivation(ArrayList<IncomingEdge> edgeList, Double biasWeight, Double biasValue)
	{
		//Boolean returner = false;
		Double summer = 0.0;
			
		for(IncomingEdge entry : edgeList)
		{
			//System.out.println("edge number "+entry.getKey()+", edge input "+entry.getValue().inputValue);
			summer += ( entry.inputValue * 
					entry.weightValue);
		}
		
		summer += biasWeight * biasValue;
		
		/*if(summer >= 0)
		{
			returner = true;
		}
		else
		{
			returner = false;
		}*/
		
		return summer;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Null, or Identity";
	}
}
