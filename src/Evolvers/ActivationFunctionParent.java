package Evolvers;
import java.util.ArrayList;
import java.util.HashMap;

import Model.IncomingEdge;

public abstract class ActivationFunctionParent
{
	public ActivationFunctionParent()
	{}
	
	public abstract Double tryActivation(ArrayList<IncomingEdge> edgeList, Double biasWeight, Double biasValue);

	public abstract String toString();
}










