package Evolvers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import Model.IncomingEdge;

public class AFsigmoid extends ActivationFunctionParent
{
	public AFsigmoid()
	{}

	@Override
	public Double tryActivation(ArrayList<IncomingEdge> edgeList,
			Double biasWeight, Double biasValue) {
		
		//Boolean returner = false;
				Double summer = 0.0;
				
				for(IncomingEdge entry : edgeList)
				{
					summer += ( entry.inputValue * entry.weightValue);
				}
				
				summer += biasWeight * biasValue;
				
				Double result = 1 / (1 + Math.exp(-1*summer));
		// TODO Auto-generated method stub
		return result;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Sigmoid";
	}
}
