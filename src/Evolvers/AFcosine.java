package Evolvers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import Model.IncomingEdge;


public class AFcosine extends ActivationFunctionParent
{
	public AFcosine()
	{}

	@Override
	public Double tryActivation(ArrayList<IncomingEdge> edgeList,
			Double biasWeight, Double biasValue) {
			
			Double summer = 0.0;
				
				for(IncomingEdge entry : edgeList)
				{
					summer += ( entry.inputValue * entry.weightValue);
				}
				
				summer += biasWeight * biasValue;
				
				Double result = Math.cos(Math.toRadians(summer));
			
		// TODO Auto-generated method stub
		return result;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Cosine";
	}
}
