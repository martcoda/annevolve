package Evolvers;
import Model.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

import Model.DataHolder;
import Model.IncomingEdge;
import Model.Log;
import Model.NeuralNetwork;
import Model.Node;


public class Evolution {
	
	private DataHolder dataholder;
	private Boolean finishedBreeding = false;

	public synchronized Boolean getFinishedBreeding() {
		return finishedBreeding;
	}

	public synchronized void setFinishedBreeding(Boolean finishedBreeding) {
		this.finishedBreeding = finishedBreeding;
	}

	public Evolution(DataHolder dataholder)
	{
		this.dataholder = dataholder;
	}
	
	public void selectParentsAndBreed()
	{
		this.finishedBreeding = false;
		Log.addToLog("Starting the stage of select parents and breed");
		HashMap<String,NeuralNetwork> nextGeneration = new HashMap<String,NeuralNetwork>();
		// First of all keep the fittest
		NeuralNetwork fittest = null;
		String fittestID = null;

		//Cycle through the ANN's and pick out the fittest one. 
		for(Entry<String,NeuralNetwork> ann : this.dataholder.getCopyOfLinearPopulation().entrySet())
		{
			// You're at the start of the search
			if(fittest == null)
			{
				fittest = new NeuralNetwork(dataholder,
						dataholder.getActivationFunction(),
						UUID.randomUUID().toString(),
						dataholder.getWeightMinimum(),
						dataholder.getWeightMaximum());
				
				fittest = this.initialiseFittest(fittest, ann.getValue());
				fittest.setOverallFitness(ann.getValue().getOverallFitness());
			}
			// Otherwise get comparing with previous
			else
			{
					NeuralNetwork temp = ann.getValue();
					Double d = ann.getValue().getOverallFitness();
					System.out.println(temp.populationMemberID);
					System.out.println(temp);
					System.out.println(d);
					//Lower fitness value is better, as 0.0 is perfect
				if(ann.getValue().getOverallFitness() 
						< fittest.getOverallFitness())
				{
					fittest = new NeuralNetwork(dataholder,dataholder.getActivationFunction(),
							ann.getKey(),
							dataholder.getWeightMinimum(),dataholder.getWeightMaximum());
					fittest = this.initialiseFittest(fittest, ann.getValue());
					fittest.setOverallFitness(ann.getValue().getOverallFitness());
				}
			}
		}
		// Elitism here by adding the fittest to next generatino 
		nextGeneration.put(fittestID,fittest);
		
		// Add 1/4 popsize clones of fittest too!!! actually dont do this
		/*for(int x = 0; x < dataholder.getLinearPopulation().size()/4; x++)
		{
			NeuralNetwork ann = new NeuralNetwork(dataholder,dataholder.getActivationFunction(),UUID.randomUUID().toString(),dataholder.getWeightMinimum(),dataholder.getWeightMaximum());
			ann = this.crossoverInitialiseChild(ann, fittest);
			nextGeneration.put(ann.populationMemberID,ann);
		}*/
		
		// Also going to use this fittest individual of the generation to populate figures for drawing graphs
		dataholder.getActualOutputs().clear(); // Empty current contents of actual outputs in dataholder.
		for(Double d : fittest.getActualOutputs())
		{
			//System.out.println("about to add to actual outputs, value is "+d);
			dataholder.addToActualOutputs(d);
			//System.out.println("actual outputs size is "+dataholder.getActualOutputs().size());
		}
		dataholder.getExpectedOutputs().clear();
		for(Double d : fittest.getExpectedOutputs())
		{
			dataholder.addToExpectedOutputs(d);
		}
		
		dataholder.setLMS(fittest.getOverallLMS());
		
		// Now tournament select 25% of the population randomly, then choose the two fittest to breed. 
		//Uggh, forget the above, will use tournament size 2 to pick parents
		while(nextGeneration.size() < dataholder.getPopulationSize())
		{
			HashMap<String,NeuralNetwork> parents = new HashMap<String,NeuralNetwork>();
			//Here breed
			for(int x = 0; x < 2; x++)
			{
				//Will use tournament size 2 to select 2 candidates and choose the fittest parent
				//...then as this for-loop cycles twice, will do again to choose a second parent
				Boolean alreadySelected = true;
				List annPopulation = new ArrayList(dataholder.getCopyOfLinearPopulation().values());
				while(alreadySelected)
				{
					int random = new Random().nextInt( dataholder.getPopulationSize().intValue() );
					System.out.println("Random number chosen for selected random parent is "+random);
					NeuralNetwork parent1 = (NeuralNetwork) annPopulation.get(random);
					int random2 = new Random().nextInt( dataholder.getPopulationSize().intValue() );
					NeuralNetwork parent2 = (NeuralNetwork) annPopulation.get(random2);
					NeuralNetwork fittestParent;
					if(parent1.getOverallFitness() < parent2.getOverallFitness())
					{//Less is better, as 0.0 is perfect
						fittestParent = parent1;
					}
					else
					{
						fittestParent = parent2;
					}
					
					if(!parents.containsKey(fittestParent.populationMemberID))
					{
						Log.addToLog("new parent added with key "+fittestParent.populationMemberID+ " and loop out of four is "+x);
						parents.put(fittestParent.populationMemberID, fittestParent);
						alreadySelected = false; // now will break out of while loop
					}
					Log.addToLog("Just selected parent"+fittestParent.populationMemberID+" and random numbers for choosing parents were "+random+" "+random2);
				}
			}
			Log.addToLog("The two chosen parents are: ");
			for(String s : parents.keySet())
			{
				Log.addToLog(s);
			}

			// Now choose the two fittest of the four parents
			/*NeuralNetwork fittestParent = null;
			NeuralNetwork secondFittestParent = null;*/

			//Cycle through the tournament-chosen parents and pick out the fittest parent. 
			/*for(NeuralNetwork ann : parents.values())
			{
				// You're at the start of the search
				if(fittestParent == null)
				{
					fittestParent = ann;
					Log.addToLog("preliminary assigned fittestparent ANN "+fittestParent.populationMemberID);
				}
				// Otherwise get comparing with previous
				else
				{	
					if(ann.getOverallFitness() > fittestParent.getOverallFitness() )
					{
						fittestParent = ann; // Set a new fittest ANN. 
						Log.addToLog("changed fittestparent to ANN "+fittestParent.populationMemberID);
					}
				}
			}*/

			// Now remove the fittest from the set, and run again to pick the second-fittest. 
			/*parents.remove(fittestParent.populationMemberID);
			//Cycle through the tournament-chosen parents and pick out the fittest two. 
			for(NeuralNetwork ann : parents.values())
			{
				// You're at the start of the search
				if(secondFittestParent == null)
				{
					secondFittestParent = ann;
					Log.addToLog("preliminary assign secondfittestparent ANN "+secondFittestParent.populationMemberID);
				}
				// Otherwise get comparing with previous
				else
				{	
					if(ann.getOverallFitness() > secondFittestParent.getOverallFitness())
					{
						secondFittestParent = ann; // Set a new fittest ANN. 
						Log.addToLog("Changed secondfittestparent ANN "+secondFittestParent.populationMemberID);
					}
				}
			}*/

			
			// Breed the two parents by using CROSSOVER for nodes 

			Random random = new Random();
			NeuralNetwork parent1 = (NeuralNetwork) parents.values().toArray()[0];
			NeuralNetwork parent2 = (NeuralNetwork) parents.values().toArray()[1];
			
			NeuralNetwork worst = (NeuralNetwork) this.dataholder.getCopyOfLinearPopulation().values().toArray()[0];
			for(NeuralNetwork nn : this.dataholder.getCopyOfLinearPopulation().values())
			{
				if(nn.getOverallFitness() > worst.getOverallFitness())
					worst = nn;
			}//Ok have now found worst ANN
			
			// If there's space for two more, create two children. 
			if(nextGeneration.size() < dataholder.getPopulationSize()-1)
			{
					NeuralNetwork child = doCrossoverWithProbabilityOneNodeEachLayer(random,parent1,parent2);
					dataholder.mutateOneANN(child);
					nextGeneration.put(child.populationMemberID, child);
					NeuralNetwork child2 = doCrossoverWithProbabilityOneNodeEachLayer(random,parent2,parent1);
					dataholder.mutateOneANN(child2);
					nextGeneration.put(child2.populationMemberID, child2);
				
			}
			// Otherwise just create one child
			else
			{
				NeuralNetwork child = doCrossoverWithProbabilityOneNodeEachLayer(random,parent1,parent2);
				dataholder.mutateOneANN(child);
				nextGeneration.put(child.populationMemberID, child);
			}

			for(String annkey : nextGeneration.keySet())
			{
				Log.addToLog("next generation currently has keys "+annkey);
			}

			Log.addToLog("Tournament round complete, nextGeneration size is "+nextGeneration.size()+" and next gen is currently "+nextGeneration.toString());
		
			/*
			//Now remove those four parents from the previous population so they can't be picked again
			HashMap<String,NeuralNetwork> previousPop = dataholder.getCopyOfLinearPopulation();
			
				if(previousPop.containsKey(parent1.populationMemberID))
				{
					previousPop.remove(parent1.populationMemberID);
				}
				if(previousPop.containsKey(parent2.populationMemberID))
				{
					previousPop.remove(parent2.populationMemberID);
				}*/
			
		}//end of while loop populating the next generation. 

		Log.addToLog("Tournament rounds finished, nextGeneration is full with size "+nextGeneration.size()+" and keys are "+nextGeneration.toString());

		dataholder.clearLinearPopulation(); // Clear old generation
		dataholder.setLinearPopulation(nextGeneration); // Set the next generation as the current one, ready for another evolutionary run. 
	
		this.finishedBreeding = true;
	}
	
	private NeuralNetwork initialiseFittest(NeuralNetwork fittest,NeuralNetwork parent1)
	{
		//Initialise first layer of child from parent1
		fittest.setActualOutputs(parent1.getActualOutputs());
		fittest.setExpectedOutputs(parent1.getExpectedOutputs());
		fittest.setDifferenceBetweenEandO(parent1.getDifferenceBetweenEandO());
		fittest.setOverallFitness(parent1.getOverallFitness());
		fittest.setOverallLMS(parent1.getOverallLMS());

		fittest.setNumFirstLayer(parent1.getNumFirstLayer());
		fittest.initialiseFirstLayerNodes();
		fittest.setNumInEachHiddenLayer(parent1.getNumbersInHiddenLayers());
		fittest.setNumHiddenLayers(parent1.getNumHiddenLayers());
		fittest.initialiseHiddenLayerNodes();
		fittest.initialiseOutputNode();
		// Now copy over the weights from parent1 to child
		for(int x=0; x < fittest.getNumFirstLayer(); x++)
		{
			fittest.getFirstLayerNodes().get(x).setBiasValue(parent1.getFirstLayerNodes().get(x).getBiasValue());
			fittest.getFirstLayerNodes().get(x).setBiasWeight( parent1.getFirstLayerNodes().get(x).getBiasWeight() );
			for(int y=0; y < fittest.getFirstLayerNodes().get(x).getIncomingEdgeList().size(); y++ )
			{
				fittest.getFirstLayerNodes().get(x).getIncomingEdgeList().get(y).weightValue = 
						parent1.getFirstLayerNodes().get(x).getIncomingEdgeList().get(y).weightValue;
			}
		}

		for(int z=0; z < fittest.getHiddenLayers().size(); z++)
		{
			for(int x=0; x < fittest.getHiddenLayers().get(z).size(); x++)
			{
				fittest.getHiddenLayers().get(z).get(x).setBiasValue(parent1.getHiddenLayers().get(z).get(x).getBiasValue());
				fittest.getHiddenLayers().get(z).get(x).setBiasWeight( parent1.getHiddenLayers().get(z).get(x).getBiasWeight() );

				for(int y=0; y < fittest.getHiddenLayers().get(z).get(x).getIncomingEdgeList().size(); y++ )
				{
					fittest.getHiddenLayers().get(z).get(x).getIncomingEdgeList().get(y).weightValue = 
					parent1.getHiddenLayers().get(z).get(x).getIncomingEdgeList().get(y).weightValue;
				}
			}
		}

		fittest.getOutputLayerNode().setBiasValue(parent1.getOutputLayerNode().getBiasValue());
		fittest.getOutputLayerNode().setBiasWeight(parent1.getOutputLayerNode().getBiasWeight());
		for(int y=0; y < fittest.getOutputLayerNode().getIncomingEdgeList().size(); y++ )
		{
			fittest.getOutputLayerNode().getIncomingEdgeList().get(y).weightValue = 
					parent1.getOutputLayerNode().getIncomingEdgeList().get(y).weightValue;
		}

		return fittest;
	}
	
	private NeuralNetwork doCrossoverWithProbabilityOneNodeEachLayer(Random random, NeuralNetwork parent1, NeuralNetwork parent2)
	{
		
		Log.addToLog("Doing crossover with probability one node each layer");
		String uniqueID = UUID.randomUUID().toString();
		NeuralNetwork child = new NeuralNetwork(dataholder,dataholder.getActivationFunction(),uniqueID,dataholder.getWeightMinimum(),dataholder.getWeightMaximum());

		
		//Initialise first layer of child from parent1
		child = this.crossoverInitialiseChild(child, parent1);
		
		boolean happens = random.nextDouble() < dataholder.getCrossoverRate();//http://stackoverflow.com/questions/10368202/java-how-do-i-simulate-probability
		
		if(happens) // do crossover
		{
			
			Log.addToLog("Crossing over single node in first layer");
			Double spinner = Math.random();
			if(spinner < 0.5) //50 50 chance of which first-layer node to replace with
			{
				child.getFirstLayerNodes().get(0).setBiasValue(parent2.getFirstLayerNodes().get(parent2.getFirstLayerNodes().size()-1).getBiasValue());
				child.getFirstLayerNodes().get(0).setBiasWeight( parent2.getFirstLayerNodes().get(parent2.getFirstLayerNodes().size()-1).getBiasWeight() );
				
				for(int x = 0; x < child.getFirstLayerNodes().get(0).getIncomingEdgeList().size(); x++)
				{
					child.getFirstLayerNodes().get(0).getIncomingEdgeList().get(x).weightValue = 
					parent2.getFirstLayerNodes().get(parent2.getFirstLayerNodes().size()-1).getIncomingEdgeList().get(x).weightValue;
				}
			}
			else
			{
				child.getFirstLayerNodes().get(0).setBiasValue(parent2.getFirstLayerNodes().get(0).getBiasValue());
				child.getFirstLayerNodes().get(0).setBiasWeight( parent2.getFirstLayerNodes().get(0).getBiasWeight() );
				
				for(int x = 0; x < child.getFirstLayerNodes().get(0).getIncomingEdgeList().size(); x++)
				{
					child.getFirstLayerNodes().get(0).getIncomingEdgeList().get(x).weightValue = 
					parent2.getFirstLayerNodes().get(0).getIncomingEdgeList().get(x).weightValue;
				}
			}
			
			// As topology can be mutated, the parents may not be a topological match, so need to be careful with indexes
			int index = 0;
			if(parent2.getHiddenLayers().size() < parent1.getHiddenLayers().size())
			{
				// There will be a mismatch in indexes, so need to just iterate enough indexes for the smaller one. 
				index = parent2.getHiddenLayers().size();
			}
			else
			{
				index = parent1.getHiddenLayers().size();
			}
			System.out.println("Number of hidden layers "+child.getHiddenLayers().size());
			for(int z = 0; z < index; z++)
			{
				Log.addToLog("Crossing over single node in hidden layer, random of "+parent2.getHiddenLayers().get(z).size());
				System.out.println("Crossing over single node in hidden layer, random of "+parent2.getHiddenLayers().size()+" "+z+" "+parent2.getHiddenLayers().get(z).size());
				
				int randomNodeNumbFromParent2 = new Random().nextInt( parent2.getHiddenLayers().get(z).size() );
				int randomNodeNumbFromParent1 = new Random().nextInt( parent1.getHiddenLayers().get(z).size());

				Log.addToLog("random node in parent1 is "+randomNodeNumbFromParent1+ "and size of parent1 hiddenlayer is "+parent1.getHiddenLayers().get(z).size());
				Log.addToLog("random node in parent2 is "+randomNodeNumbFromParent2+ "and size of parent2 hiddenlayer is "+parent2.getHiddenLayers().get(z).size());

				child.getHiddenLayers().get(z).get(randomNodeNumbFromParent1).setBiasValue(parent2.getHiddenLayers().get(z).get(randomNodeNumbFromParent2).getBiasValue());
				child.getHiddenLayers().get(z).get(randomNodeNumbFromParent1).setBiasWeight( parent2.getHiddenLayers().get(z).get(randomNodeNumbFromParent2).getBiasWeight() );
				
				// Crossover a random hidden-layer node from parent 2 to child
				
				int randomEdgeeNumbFromParent2 = new Random().nextInt( parent2.getHiddenLayers().get(z).get(randomNodeNumbFromParent2).getIncomingEdgeList().size());
				int randomEdgeeNumbFromchild = new Random().nextInt( child.getHiddenLayers().get(z).get(randomNodeNumbFromParent1).getIncomingEdgeList().size());

				
				for(int x = 0; x < child.getHiddenLayers().get(z).get(randomNodeNumbFromParent1).getIncomingEdgeList().size(); x++)
				{
					child.getHiddenLayers().get(z).get(randomNodeNumbFromParent1).getIncomingEdgeList().get(randomEdgeeNumbFromchild).weightValue = 
					parent2.getHiddenLayers().get(z).get(randomNodeNumbFromParent2).getIncomingEdgeList().get(randomEdgeeNumbFromParent2).weightValue;
				}
			}
			
			if(child.getOutputLayerNode().getIncomingEdgeList().size() == parent2.getOutputLayerNode().getIncomingEdgeList().size())
			{
				Double coin = Math.random();
				if(coin < 0.2) // 20% chance of crossing over output node. 
				{
					for(int y=0; y < child.getOutputLayerNode().getIncomingEdgeList().size(); y++ )
					{
						child.getOutputLayerNode().getIncomingEdgeList().get(y).weightValue = 
						parent2.getOutputLayerNode().getIncomingEdgeList().get(y).weightValue;
					}
				}
				
			}
			else{
				//output node not suitable for crossover as different numbers of incoming edges...
			}
			
		}
		else // don't crossover
		{

		}
		
		return child;
	}
	
	private NeuralNetwork crossoverInitialiseChild(NeuralNetwork child,NeuralNetwork parent1)
	{
		//Initialise first layer of child from parent1
		child.setNumFirstLayer(parent1.getNumFirstLayer());
		child.initialiseFirstLayerNodes();
		child.setNumInEachHiddenLayer(parent1.getNumbersInHiddenLayers());
		child.setNumHiddenLayers(parent1.getNumHiddenLayers());
		child.initialiseHiddenLayerNodes();
		child.initialiseOutputNode();
		// Now copy over the weights from parent1 to child
		for(int x=0; x < child.getNumFirstLayer(); x++)
		{
			child.getFirstLayerNodes().get(x).setBiasValue(parent1.getFirstLayerNodes().get(x).getBiasValue());
			child.getFirstLayerNodes().get(x).setBiasWeight( parent1.getFirstLayerNodes().get(x).getBiasWeight() );
			for(int y=0; y < child.getFirstLayerNodes().get(x).getIncomingEdgeList().size(); y++ )
			{
				child.getFirstLayerNodes().get(x).getIncomingEdgeList().get(y).weightValue = 
				parent1.getFirstLayerNodes().get(x).getIncomingEdgeList().get(y).weightValue;
			}
		}
		
		for(int z=0; z < child.getNumHiddenLayers(); z++)
		{
			for(int x=0; x < child.getHiddenLayers().get(z).size(); x++)
			{
				child.getHiddenLayers().get(z).get(x).setBiasValue(parent1.getHiddenLayers().get(z).get(x).getBiasValue());
				child.getHiddenLayers().get(z).get(x).setBiasWeight( parent1.getHiddenLayers().get(z).get(x).getBiasWeight() );
				
				for(int y=0; y < child.getHiddenLayers().get(z).get(x).getIncomingEdgeList().size(); y++ )
				{
					child.getHiddenLayers().get(z).get(x).getIncomingEdgeList().get(y).weightValue = 
					parent1.getHiddenLayers().get(z).get(x).getIncomingEdgeList().get(y).weightValue;
				}
			}
		}
		
		child.getOutputLayerNode().setBiasValue(parent1.getOutputLayerNode().getBiasValue());
		child.getOutputLayerNode().setBiasWeight(parent1.getOutputLayerNode().getBiasWeight());
		for(int y=0; y < child.getOutputLayerNode().getIncomingEdgeList().size(); y++ )
		{
			child.getOutputLayerNode().getIncomingEdgeList().get(y).weightValue = 
			parent1.getOutputLayerNode().getIncomingEdgeList().get(y).weightValue;
		}
		
		return child;
	}
	
	private ArrayList<NeuralNetwork> doCrossoverWithProbabilityAllHidden(Random random, NeuralNetwork parent1, NeuralNetwork parent2)
	{
		ArrayList<NeuralNetwork> returner = new ArrayList<NeuralNetwork>();
		String uniqueID = UUID.randomUUID().toString();
		String uniqueID2 = UUID.randomUUID().toString();
		NeuralNetwork child1 = new NeuralNetwork(dataholder,dataholder.getActivationFunction(),uniqueID,dataholder.getWeightMinimum(),dataholder.getWeightMaximum());
		NeuralNetwork child2 = new NeuralNetwork(dataholder,dataholder.getActivationFunction(),uniqueID2,dataholder.getWeightMinimum(),dataholder.getWeightMaximum());

		child1 = this.crossoverInitialiseChild(child1, parent1);
		child2 = this.crossoverInitialiseChild(child2, parent2);
		
		/*boolean happens = random.nextDouble() < dataholder.getCrossoverRate();//http://stackoverflow.com/questions/10368202/java-how-do-i-simulate-probability
		if(happens) // do crossover
		{
			Log.addToLog("Crossover happening for entire first hidden layer");
			//First check that size is same
			
			if(child1.getHiddenLayers().get(0).size() == parent2.getHiddenLayers().get(0).size())
			{
					for(int n = 0; n < child1.getHiddenLayers().get(0).size(); n++)
					{
						child1.getHiddenLayers().get(0).get(n).setBiasValue(parent2.getHiddenLayers().get(0).get(n).getBiasValue());
						child1.getHiddenLayers().get(0).get(n).setBiasWeight( parent2.getHiddenLayers().get(0).get(n).getBiasWeight() );
						
						for(int x = 0; x < child1.getHiddenLayers().get(0).get(n).getIncomingEdgeList().size(); x++)
						{
							child1.getHiddenLayers().get(0).get(n).getIncomingEdgeList().get(x).weightValue = 
							parent2.getHiddenLayers().get(0).get(n).getIncomingEdgeList().get(x).weightValue;
						}
					}

			}
			else
			{
				//hidden layer sizes are different from previous mutation, so don't bother. 
			}

		}
		else // don't crossover
		{
		}*/

		return returner;
	}
}
