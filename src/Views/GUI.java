package Views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Evolvers.AFcosine;
import Evolvers.AFgaussian;
import Evolvers.AFhyperbolicTangent;
import Evolvers.AFnull;
import Evolvers.AFsigmoid;
import Model.DataHolder;

public class GUI extends JFrame implements Observers{
	
	//instance variables
	public static final String NL = System.getProperty("line.separator");
	private DecimalFormat decimalFormat = new DecimalFormat("#.####");
	
	ArrayList<JTextArea> jTextAreaList = new ArrayList<JTextArea>();
	
	JComboBox functionToApproximate;
	JComboBox startingActivation;
	JComboBox whatToEvolve;

	JTextArea messages;
	JScrollPane scrollForMessages;
	JTextArea numberEvolutions;
	JTextArea popSize;
	JTextArea crossoverRate;
	JTextArea mutationRate;
	JTextArea weightMin;
	JTextArea weightMax;
	JButton buttonDefaults;
	JButton buttonSubmitInitialValues;	
	JTextArea jTextArea;
	JPanel top;
	JPanel middlePanel;
	JScrollPane middleScrollPane;
	JTextArea middle;
	JPanel bottom;
	JPanel jPanel;
	JButton slowButton;
	JButton mediumButton;
	JButton fastButton;
	
	JTextArea bottomTextArea;
	
	private DataHolder dataholder;
	private ArrayList<JTextArea> desks = new ArrayList<JTextArea>();
	
	//constructor
	public GUI(DataHolder dataholder){

		decimalFormat.setRoundingMode(RoundingMode.CEILING);
		this.dataholder = dataholder;
		dataholder.addObserver(this);//important for this object to get notified of updates
	    this.setSize(800,650);
	    this.setLocation(10,10);
	    this.setTitle("Function Approximation by Evolving ANNs");
        this.setVisible(true);
        this.setLayout(new BorderLayout());
        
        setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        addAreasToFrame();
        this.createGleanArea();

        this.revalidate();
	}

	/**
	 * Sets how the frame will be set out 
	 */
	private void addAreasToFrame() {
		// TODO Auto-generated method stub
		
		top = new JPanel(new GridLayout(1,3));
		top.setBorder(new EmptyBorder(2,2,2,2));
		top.setBorder(BorderFactory.createLineBorder(Color.black));
		this.add(top,BorderLayout.NORTH);
		
		middle = new JTextArea();
		middleScrollPane = new JScrollPane(middle);
		middleScrollPane.setOpaque(true);
		//middlePanel.add(middleScrollPane);
		this.add(middleScrollPane,BorderLayout.CENTER);
		
		bottom = new JPanel(new FlowLayout());
		bottom.setSize(100,50);
		bottom.setBorder(new EmptyBorder(2,2,2,2));
		bottom.setBorder(BorderFactory.createLineBorder(Color.black));
		bottomTextArea = new JTextArea();
		bottom.add(bottomTextArea);
		//thankyou https://docs.oracle.com/javase/tutorial/uiswing/components/border.html
		this.add(bottom, BorderLayout.SOUTH);
	}

	private void createGleanArea() {
		
		JPanel paramsToBeEvolved = new JPanel(new GridLayout(3,1));
		JPanel variables = new JPanel(new GridLayout(6,2));
		JPanel under = new JPanel(new GridLayout(2,1));
		
		String[] functionApproximateChoice = {"Choose which function to approximate...","LINEAR","CUBIC","SINE","TANH","XOR","COMPLEX"};
		functionToApproximate = new JComboBox(functionApproximateChoice);
		functionToApproximate.setSelectedIndex(0);
		
		String[] activationFunctionContent = {"Choose starting activation function...","null","sigmoid","hyperbolic tangent","cosine","gaussian"};
		startingActivation = new JComboBox(activationFunctionContent);
		startingActivation.setSelectedIndex(0);
		
		String[] paramsEvolved = {"Choose What To Evolve...","Weights","Activation Function","Both"};
		whatToEvolve = new JComboBox(paramsEvolved);
		whatToEvolve.setSelectedIndex(0);
		paramsToBeEvolved.add(functionToApproximate);
		paramsToBeEvolved.add(startingActivation);
		paramsToBeEvolved.add(whatToEvolve);

		JLabel evolLabl = new JLabel("Number Evolutions");
		evolLabl.setBorder(BorderFactory.createLineBorder(Color.black));
		numberEvolutions = new JTextArea();
		numberEvolutions.setBorder(BorderFactory.createLineBorder(Color.black));
		variables.add(evolLabl);
		variables.add(numberEvolutions);
		
		JLabel popsizeLabl = new JLabel("Pop size");
		popsizeLabl.setBorder(BorderFactory.createLineBorder(Color.black));
		popSize = new JTextArea();
		popSize.setBorder(BorderFactory.createLineBorder(Color.black));
		variables.add(popsizeLabl);
		variables.add(popSize);
		
		JLabel crossLabel = new JLabel("Crossover rate");
		crossLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		crossoverRate = new JTextArea();
		crossoverRate.setBorder(BorderFactory.createLineBorder(Color.black));
		variables.add(crossLabel);
		variables.add(crossoverRate);
		
		JLabel mutateLabel = new JLabel("Mutation rate");
		mutateLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		mutationRate = new JTextArea();
		mutationRate.setBorder(BorderFactory.createLineBorder(Color.black));
		variables.add(mutateLabel);
		variables.add(mutationRate);
		
		JLabel weiMinLabel = new JLabel("Weights min");
		weiMinLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		weightMin = new JTextArea();
		weightMin.setBorder(BorderFactory.createLineBorder(Color.black));
		variables.add(weiMinLabel);
		variables.add(weightMin);
		
		JLabel weiMaxLabel = new JLabel("Weights max");
		weiMaxLabel.setBorder(BorderFactory.createLineBorder(Color.black));
		weightMax = new JTextArea();
		weightMax.setBorder(BorderFactory.createLineBorder(Color.black));
		variables.add(weiMaxLabel);
		variables.add(weightMax);
		
		messages = new JTextArea("messages show here");
		messages.setForeground(Color.RED);
		messages.setBackground(Color.BLACK);
		scrollForMessages = new JScrollPane(messages);
		scrollForMessages.setOpaque(true);
		
		JPanel buttonPanel = new JPanel(new GridLayout(1,2));
		
		buttonDefaults = new JButton("Fill in defaults");
		
		buttonSubmitInitialValues = new JButton("Submit");
		
		buttonPanel.add(buttonDefaults);
		buttonPanel.add(buttonSubmitInitialValues);
		
		variables.setBorder(BorderFactory.createLineBorder(Color.black));
		
		under.add(scrollForMessages);
		under.add(buttonPanel);
		
		under.setBorder(BorderFactory.createLineBorder(Color.black));
		
		top.add(paramsToBeEvolved);
		top.add(variables);
		top.add(under);
	}		
	/**
	 * This creates Worker panels based on how many Worker objects there are
	 */
	
	public void addDefaultsButtonListener(ActionListener al)
	{
		this.buttonDefaults.addActionListener(al);
	}
	
	public void addGleanButtonListener(ActionListener al) {
		this.buttonSubmitInitialValues.addActionListener(al);
    }
	
	public void disableSubmitButton()
	{
		this.buttonSubmitInitialValues.setEnabled(false); // Disable button to stop people pressing until cycles have finished. 
	}

	public void fillInDefaultValues(ActionEvent e)
	{
		if(e.getSource() == this.buttonDefaults)
		{
			this.numberEvolutions.setText("50");
			this.popSize.setText("100");
			this.crossoverRate.setText("0.5");
			this.mutationRate.setText("0.05");
			this.weightMin.setText("-5");
			this.weightMax.setText("5");
		}
	}
	
	public Boolean sendInitialVarsToModel(ActionEvent e)
	{
		if(e.getSource() == this.buttonSubmitInitialValues){
				
			// First get which function to approximate
			System.out.println("TESTING TESTIN TESTING");
			String whichFunctionToApproximate = (String) this.functionToApproximate.getSelectedItem();
			System.out.println("function to approx is "+whichFunctionToApproximate);
			
			String themessages = "";
			if(whichFunctionToApproximate.equals("LINEAR"))
			{
				dataholder.setFunctionToApprox("1in_linear.txt");
				themessages+="Approximation of function: "+whichFunctionToApproximate+NL;
			}
			else if(whichFunctionToApproximate.equals("CUBIC"))
			{
				dataholder.setFunctionToApprox("1in_cubic.txt");
				themessages+="Approximation of function: "+whichFunctionToApproximate+NL;
			}
			else if(whichFunctionToApproximate.equals("SINE"))
			{
				dataholder.setFunctionToApprox("1in_sine.txt");
				themessages+="Approximation of function: "+whichFunctionToApproximate+NL;
			}
			else if(whichFunctionToApproximate.equals("TANH"))
			{
				dataholder.setFunctionToApprox("1in_tanh.txt");
				themessages+="Approximation of function: "+whichFunctionToApproximate+NL;
			}
			else if(whichFunctionToApproximate.equals("XOR"))
			{
				dataholder.setFunctionToApprox("2in_xor.txt");
				themessages+="Approximation of function: "+whichFunctionToApproximate+NL;
			}
			else if(whichFunctionToApproximate.equals("COMPLEX"))
			{
				dataholder.setFunctionToApprox("2in_complex.txt");
				themessages+="Approximation of function: "+whichFunctionToApproximate+NL;
			}
			else
			{
				dataholder.setFunctionToApprox("1in_linear.txt");
				themessages+="Approximation of default function: LINEAR"+NL;
			}
			
			// Next get which activation function to begin with
			String whichActivationFunction = (String) this.startingActivation.getSelectedItem();
			if(whichActivationFunction.equals("null"))
			{
				dataholder.setActivationFunction(new AFnull());
				themessages+="Activation function: "+whichActivationFunction+NL;
			}
			else if(whichActivationFunction.equals("sigmoid"))
			{
				dataholder.setActivationFunction(new AFsigmoid());
				themessages+="Activation function: "+whichActivationFunction+NL;
			}
			else if(whichActivationFunction.equals("hyperbolic tangent"))
			{
				dataholder.setActivationFunction(new AFhyperbolicTangent());
				themessages+="Activation function: "+whichActivationFunction+NL;
			}
			else if(whichActivationFunction.equals("cosine"))
			{
				dataholder.setActivationFunction(new AFcosine());
				themessages+="Activation function: "+whichActivationFunction+NL;
			}
			else if(whichActivationFunction.equals("gaussian"))
			{
				dataholder.setActivationFunction(new AFgaussian());
				themessages+="Activation function: "+whichActivationFunction+NL;
			}
			else
			{
				dataholder.setActivationFunction(new AFnull());
				themessages+="Activation function will be default: null (also called identity)"+NL;
			}
			
			// Next get the choice of what to evolve 
			String whatEvolve = (String) this.whatToEvolve.getSelectedItem();
			
			if(whatEvolve.equals("Weights"))
			{
				dataholder.setEvolveWeights(true);
				themessages+="You will evolve "+whatEvolve;
			}
			else if(whatEvolve.equals("Activation Function"))
			{
				dataholder.setEvolveActivation(true);
				themessages+="You will evolve "+whatEvolve;
			}
			else if(whatEvolve.equals("Both"))
			{
				dataholder.setEvolveActivation(true);
				dataholder.setEvolveWeights(true);
				themessages+="You will evolve both weights and activation function";
			}
			else // If no choice was made, default will be just weights. 
			{
				dataholder.setEvolveWeights(true);
				themessages+="You will evolve the default of only weights";
			}
			
			// Next get the params entered in the text boxes. 
			
			this.middle.setText("");
			try {
				
				String evols = (this.numberEvolutions.getText()).trim();
				if(evols.equals(""))
				{
					themessages += NL+"evolutions was empty";
				}
				else{
					themessages += NL+dataholder.setNumberEvolutions((Integer)Integer.parseInt(evols));
				}
				String popsize = (this.popSize.getText()).trim();
				if(popsize.equals(""))
				{
					themessages += NL+"popsize was empty";
					
				}
				else{
					themessages += NL+dataholder.setPopulationSize((Double)Double.parseDouble(popsize));
				}
				
				String crossRate = (this.crossoverRate.getText()).trim();
				if(crossRate.equals(""))
				{
					themessages += NL+"crossover rate was empty";
				}
				else
				{
					themessages += NL+dataholder.setCrossoverRate((Double)Double.parseDouble(crossRate));
				}
				
				String mutateRate = (this.mutationRate.getText()).trim();
				if(mutateRate.equals(""))
				{
					themessages += NL+"mutation rate was empty";
				}
				else
				{
					themessages += NL+dataholder.setMutationRate((Double)Double.parseDouble(mutateRate));
				}
				
				String minWeight = (this.weightMin.getText()).trim();
				if(minWeight.equals(""))
				{
					themessages += NL+"min weight was empty";
				}
				else
				{
					themessages += NL+dataholder.setMinimumWeight((Double)Double.parseDouble(minWeight));
				}
				
				String maxWeight = (this.weightMax.getText()).trim();
				if(maxWeight.equals(""))
				{
					themessages += NL+"max weight was empty";
				}
				else
				{
					themessages += NL+dataholder.setMaximumWeight((Double)Double.parseDouble(maxWeight));
				}	
				
				this.messages.setText(themessages);
				this.revalidate();
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return true;
	}
	
	public void update() 
	{
		this.middle.setText(dataholder.getOverallFitnessValuesOneLine()+NL+this.middle.getText());
		
		//this.middleScrollPane.revalidate();
		middle.revalidate();
		try
		{
		bottomTextArea.setText("Fittest ever had overall fitness of: "+dataholder.getFittestEver().getOverallFitness().toString());
		}
		catch(NullPointerException npe)
		{npe.printStackTrace();}
		bottomTextArea.revalidate();
	}
}
