package Views;
/* --------------------
* Function2DDemo1.java
* --------------------
* (C) Copyright 2007, by Object Refinery Limited.
*
*/
import java.util.ArrayList;

import Model.*;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.DomainOrder;
import org.jfree.data.function.Function2D;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.XYZDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.chart.JFreeChart.*;

/**
* An example showing how to plot a simple function in JFreeChart.  Because
* JFreeChart displays discrete data rather than plotting functions, you need
* to create a dataset by sampling the function values.
*/
public class Grapher extends ApplicationFrame implements Observers, XYZDataset {

	private DataHolder dataholder;
	JFreeChart chart;
    /**
     * Creates a new demo.
     *
     * @param title  the frame title.
     */
    public Grapher(String title, DataHolder dataholder) {
        super(title);
        this.setLocation(350,100);
        this.dataholder = dataholder;
        this.dataholder.addObserver(this);
        JPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
    }
   
    /**
     * Creates a chart.
     *
     * @param dataset  the dataset.
     *
     * @return A chart instance.
     */
    private static JFreeChart createChart(XYDataset dataset) {
        // create the chart...
        JFreeChart chart = ChartFactory.createXYLineChart(
            "Function Approximation ",       // chart title
            "X",                      // x axis label
            "Y",                      // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL, 
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.getDomainAxis().setLowerMargin(0.0);
        plot.getDomainAxis().setUpperMargin(2.0);
        return chart;
    }
   
    /**
     * Creates a sample dataset.
     *
     * @return A sample dataset.
     */
    public void update()
    {//dataset example code found at http://www.codejava.net/java-se/graphics/using-jfreechart-to-draw-xy-line-chart-with-xydataset
    	
    	//System.out.println("in grapher update");
    	
    	
    	JPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        this.getContentPane().removeAll();
        setContentPane(chartPanel);
        this.revalidate();
    	chart.fireChartChanged();
    }
    
    public XYDataset create3dDataset()
    {
    	XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries actual = new XYSeries("Actual");
        XYSeries expected = new XYSeries("Expected");
        //System.out.println("in createdataset just about to add values, actual outputs size is"+dataholder.getActualOutputs().size());
        ArrayList<Double[]> temp = Inputs.getFunctionsData().get(dataholder.getFunctionToApprox());
        for(int x = 0;x < dataholder.getActualOutputs().size(); x++)
        {
        	//System.out.println("actual x and y "+temp.get(x)[0]+" "+dataholder.getActualOutputs().get(x));
        	//First value in each line of input will be the x input, actual output will be y
        	actual.add(temp.get(x)[0],dataholder.getActualOutputs().get(x));
        }
        
        for(int x = 0;x < dataholder.getActualOutputs().size(); x++)
        {
        	//System.out.println("expected x and y "+temp.get(x)[0]+" "+dataholder.getExpectedOutputs().get(x));
        	//First value in each line of input will be the x input, actual output will be y
        	expected.add(temp.get(x)[0],dataholder.getExpectedOutputs().get(x));
        }
     
     
        dataset.addSeries(actual);
        dataset.addSeries(expected);
        return dataset;
    	
    }
    public XYDataset createDataset() {
    	XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries actual = new XYSeries("Actual");
        XYSeries expected = new XYSeries("Expected");
        //System.out.println("in createdataset just about to add values, actual outputs size is"+dataholder.getActualOutputs().size());
        ArrayList<Double[]> temp = Inputs.getFunctionsData().get(dataholder.getFunctionToApprox());
        for(int x = 0;x < dataholder.getActualOutputs().size(); x++)
        {
        	//System.out.println("actual x and y "+temp.get(x)[0]+" "+dataholder.getActualOutputs().get(x));
        	//First value in each line of input will be the x input, actual output will be y
        	actual.add(temp.get(x)[0],dataholder.getActualOutputs().get(x));
        }
        
        for(int x = 0;x < dataholder.getActualOutputs().size(); x++)
        {
        	//System.out.println("expected x and y "+temp.get(x)[0]+" "+dataholder.getExpectedOutputs().get(x));
        	//First value in each line of input will be the x input, actual output will be y
        	expected.add(temp.get(x)[0],dataholder.getExpectedOutputs().get(x));
        }
     
     
        dataset.addSeries(actual);
        dataset.addSeries(expected);
        return dataset;
        
    }

    /**
     * Creates a panel for the demo (used by SuperDemo.java).
     *
     * @return A panel.
     */
    public JPanel createDemoPanel() {
    	
    	if(dataholder.getFunctionToApprox().equals("2in_xor.txt") ||dataholder.getFunctionToApprox().equals("2in_complex.txt") )
    	{
    		chart = createChart(this.create3dDataset());
            return new ChartPanel(chart);
    	}
    	else
    	{
    		chart = createChart(this.createDataset());
            return new ChartPanel(chart);
    	}
        
    }

	public DomainOrder getDomainOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getItemCount(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Number getX(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public double getXValue(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Number getY(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public double getYValue(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getSeriesCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Comparable getSeriesKey(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public int indexOf(Comparable arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void addChangeListener(DatasetChangeListener arg0) {
		// TODO Auto-generated method stub
		
	}

	public DatasetGroup getGroup() {
		// TODO Auto-generated method stub
		return null;
	}

	public void removeChangeListener(DatasetChangeListener arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setGroup(DatasetGroup arg0) {
		// TODO Auto-generated method stub
		
	}

	public Number getZ(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public double getZValue(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

   
}