package Views;
/* --------------------
* Function2DDemo1.java
* --------------------
* (C) Copyright 2007, by Object Refinery Limited.
*
*/
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import Model.*;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.function.Function2D;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.XYZDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.chart.JFreeChart.*;

/**
* An example showing how to plot a simple function in JFreeChart.  Because
* JFreeChart displays discrete data rather than plotting functions, you need
* to create a dataset by sampling the function values.
*/
public class GraphLMS extends ApplicationFrame implements Observers {

	private DecimalFormat decimalFormat = new DecimalFormat("#.####");
	private DataHolder dataholder;
	JFreeChart chart;
	JPanel content;
	private Double currentX = 0.0;
	XYSeriesCollection datasetFittest = new XYSeriesCollection();
	XYSeries lmsFittest = new XYSeries("LMSfittest");
	
	XYSeriesCollection dataset = new XYSeriesCollection();
	XYSeries lmsAverage = new XYSeries("LMSaverage");
    /**
     * Creates a new demo.
     *
     * @param title  the frame title.
     */
    public GraphLMS(String title, DataHolder dataholder) {
        super(title);
        //this.setSize(550,300);
        this.setLocation(450,300);
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        this.dataholder = dataholder;
        this.dataholder.addObserver(this);
        this.dataset.addSeries(lmsFittest);
        JPanel chartPanel =  new ChartPanel(createChart(null));
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        
        content = new JPanel(new FlowLayout());
        content.add(chartPanel);
        setContentPane(content);
    }
   
    /**
     * Creates a chart.
     *
     * @param dataset  the dataset.
     *
     * @return A chart instance.
     */
    private static JFreeChart createChart(XYDataset dataset) {
        // create the chart...
        JFreeChart chart = ChartFactory.createXYLineChart(
            "LMS",       // chart title
            "Generations",                      // x axis label
            "Y",                      // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL, 
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.getDomainAxis().setLowerMargin(0.0);
        plot.getDomainAxis().setUpperMargin(2.0);
        return chart;
    }
   
    /**
     * Creates a sample dataset.
     *
     * @return A sample dataset.
     */
    public void update()
    {//dataset example code found at http://www.codejava.net/java-se/graphics/using-jfreechart-to-draw-xy-line-chart-with-xydataset
    	System.out.println("in update of graphLMS");
    	JPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        this.getContentPane().removeAll();
        JTextArea fittestText = new JTextArea();
    	fittestText.setText("LMS: "+decimalFormat.format(dataholder.getLMS()).toString());
    	fittestText.setLocation(10, 10);
    	content.removeAll();
    	content.add(fittestText);
    	content.add(chartPanel);
    	setContentPane(content);
        //this.add(fittestText);
        this.revalidate();
    	chart.fireChartChanged();
    }
    
    public XYDataset create3dDataset()
    {
    	System.out.println("lms is "+dataholder/*.getFittestEver()*/.getLMS());
    	lmsFittest.add(currentX,dataholder./*getFittestEver().*/getLMS());
    	currentX++;
    
    return dataset;
    }
    public XYDataset createDataset() {
        	
            System.out.println("lms is "+dataholder.getLMS());
            	lmsFittest.add(currentX,dataholder.getLMS()/*.getFittestEver().getOverallLMS()*/);
            	currentX++;
           
            return dataset;   
    }

    /**
     * Creates a panel for the demo (used by SuperDemo.java).
     *
     * @return A panel.
     */
    public JPanel createDemoPanel() {
    	if(dataholder.getFunctionToApprox().equals("2in_xor.txt") ||dataholder.getFunctionToApprox().equals("2in_complex.txt") )
    	{
    		chart = createChart(this.create3dDataset());
            return new ChartPanel(chart);
    	}
    	else
    	{
    		chart = createChart(this.createDataset());
            return new ChartPanel(chart);
    	}
    } 
}