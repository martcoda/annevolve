package Model;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.RoundingMode;

import Evolvers.AFcosine;
import Evolvers.AFgaussian;
import Evolvers.AFhyperbolicTangent;
import Evolvers.AFnull;
import Evolvers.AFsigmoid;
import Evolvers.ActivationFunctionParent;

public class NeuralNetwork implements Runnable// For approximation of the linear function. 
{
	private ActivationFunctionParent activationFunction;

	private DataHolder dataholder;
	private Node outputNode;
	//private ArrayList<Integer> inputs = new ArrayList<Integer>();
	//private ArrayList<Double> inputValues = new ArrayList<Double>();
	public String populationMemberID = null;
	private String functionToApproximate;

	private Boolean initialHiddenLayerNumsdone = false;
	private Boolean initialised = false; // Gets set to true after outputnode initialised. 
	private Boolean mutateWeights = false; // Used to set whether or not to use this for mutation. 
	private Boolean mutateActivation = false;
	private Boolean finishedMutating = false;

	private ArrayList<Node> firstLayer = new ArrayList<Node>();
	private ArrayList<ArrayList<Node>> hiddenLayers = new ArrayList<ArrayList<Node>>();
	private ArrayList<Double> expectedOutputs = new ArrayList<Double>();
	private ArrayList<Double> differenceBetweenEandO = new ArrayList<Double>();
	private ArrayList<Node> hiddenLayer = new ArrayList<Node>();

	private Integer evolutionCycle;
	private Integer numInputs;
	private Integer numFirstLayer = 2;
	private ArrayList<Integer> numInEachHiddenLayer = new ArrayList<Integer>();

	private Integer numHiddenLayers = 1;

	public synchronized void setNumHiddenLayers(Integer numHiddenLayers) {
		this.numHiddenLayers = numHiddenLayers;
	}

	private Double expectedOutputValue;
	private Double weightMinimum;
	private Double weightMaximum;
	Double OverallFitness; //0.0 is perfect
	Double overallLMS = 0.0;

	private DecimalFormat decimalFormat = new DecimalFormat("#.####");
	private Random random = new Random();

	// Getters and setters. 
	public ArrayList<Node> getFirstLayerNodes()
	{
		return this.firstLayer;
	}

	public Node getOutputLayerNode()
	{
		return this.outputNode;
	}

	public Boolean setFirstLayerNodes(ArrayList<Node> importedFirstLayer)
	{
		this.firstLayer = importedFirstLayer;
		return true;
	}

	public Boolean setHiddenLayerNodes(ArrayList<Node> hiddenLayer)
	{
		this.hiddenLayer = hiddenLayer;
		return true;
	}

	public ArrayList<Integer> getNumbersInHiddenLayers()
	{
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for(ArrayList<Node> hiddenLayer : this.hiddenLayers)
		{
			nums.add(hiddenLayer.size());
		}
		return nums;
	}

	public Boolean setOutputLayerNode(Node outputNode)
	{
		this.outputNode = outputNode;
		return true;
	}


	public synchronized Double getOverallFitness() {
		return OverallFitness;
	}

	public synchronized void setOverallFitness(Double overallFitness) {
		OverallFitness = overallFitness;
	}

	public synchronized Boolean getFinishedMutating() {
		return finishedMutating;
	}

	public synchronized void setFinishedMutating(Boolean finishedMutating) {
		this.finishedMutating = finishedMutating;
	}

	public synchronized String getFunctionToApproximate() {
		return functionToApproximate;
	}

	public synchronized void setFunctionToApproximate(String functionToApproximate) {
		this.functionToApproximate = functionToApproximate;
	}

	public synchronized ActivationFunctionParent getActivationFunction() {
		return activationFunction;
	}

	public synchronized void setActivationFunction(
			ActivationFunctionParent activationFunction) {
		this.activationFunction = activationFunction;
	}

	public synchronized Node getOutputNode() {
		return outputNode;
	}

	public synchronized void setOutputNode(Node outputNode) {
		this.outputNode = outputNode;
	}

	public synchronized ArrayList<Double> getDifferenceBetweenEandO() {
		return differenceBetweenEandO;
	}

	public synchronized void setDifferenceBetweenEandO(
			ArrayList<Double> importeddifferenceBetweenEandO) {
		for(Double d : importeddifferenceBetweenEandO)
		{
			this.differenceBetweenEandO.add(d);
		}
	}

	public synchronized Double getOverallLMS() {
		return overallLMS;
	}

	public synchronized void setOverallLMS(Double overallLMS) {
		this.overallLMS = overallLMS;
	}

	public synchronized ArrayList<Double> getExpectedOutputs() {
		return expectedOutputs;
	}

	public synchronized void setExpectedOutputs(ArrayList<Double> importedexpectedOutputs) 
	{
		for(Double d: importedexpectedOutputs)
		{
			this.expectedOutputs.add(d);
		}
	}

	private ArrayList<Double> actualOutputs = new ArrayList<Double>();

	public synchronized ArrayList<Double> getActualOutputs() {
		return actualOutputs;
	}

	public synchronized void setActualOutputs(ArrayList<Double> importedactualOutputs) {
		for(Double d : importedactualOutputs)
		{
			this.actualOutputs.add(d);
		}
	}

	public synchronized Integer getEvolutionCycle() {
		return evolutionCycle;
	}

	public synchronized void setEvolutionCycle(Integer evolutionCycle) {
		this.evolutionCycle = evolutionCycle;
	}

	public synchronized Integer getNumHiddenLayers() {
		return numHiddenLayers;
	}

	public synchronized ArrayList<Integer> getNumInEachHiddenLayer() {
		return numInEachHiddenLayer;
	}

	public synchronized void setNumInEachHiddenLayer(
			ArrayList<Integer> importnumInEachHiddenLayer) {
		this.numInEachHiddenLayer.clear();
		for(Integer i : importnumInEachHiddenLayer)
		{
			this.numInEachHiddenLayer.add(i);
			System.out.println("Just added num for a hidden layer");
		}

	}

	public synchronized Integer getNumInputs() {
		return numInputs;
	}

	public synchronized void setNumInputs(Integer numInputs) {
		this.numInputs = numInputs;
	}

	public synchronized Integer getNumFirstLayer() {
		return numFirstLayer;
	}

	public synchronized void setNumFirstLayer(Integer numFirstLayer) {
		this.numFirstLayer = numFirstLayer;
	}

	public synchronized ArrayList<ArrayList<Node>> getHiddenLayers() {
		return hiddenLayers;
	}

	public synchronized void setHiddenLayers(ArrayList<ArrayList<Node>> hiddenLayers) {
		this.hiddenLayers = hiddenLayers;
	}

	public synchronized ArrayList<Node> getHiddenLayer() {
		return hiddenLayer;
	}

	public synchronized void setHiddenLayer(ArrayList<Node> hiddenLayer) {
		this.hiddenLayer = hiddenLayer;
	}

	// Constructor. 
	public NeuralNetwork(DataHolder dataholder, ActivationFunctionParent af, String populationMemberID, Double weightMin, Double weightMax)
	{
		this.setActivationFunction(af);
		if(!this.initialHiddenLayerNumsdone)
		{
			for(int x = 0; x < this.numHiddenLayers; x++)
			{
				numInEachHiddenLayer.add(5); // Set the integer for num nodes in each hidden layer
			}
			this.initialHiddenLayerNumsdone = true;
		}

		decimalFormat.setRoundingMode(RoundingMode.CEILING);
		this.populationMemberID = populationMemberID;
		this.weightMinimum = weightMin;
		this.weightMaximum = weightMax;
		this.dataholder = dataholder;
		if(dataholder.getFunctionToApprox().equals("2in_xor.txt") || dataholder.getFunctionToApprox().equals("2in_complex.txt"))
		{
			this.numInputs = 2;
		}
		else
		{
			this.numInputs = 1;
		}
	}

	// Used by thread to run this NeuralNetwork and all others in population simultaneously. 
	public void run() {
		// TODO Auto-generated method stub
		// Initialise All Nodes in first layers, hidden layer and the output node. 
		this.setFinishedMutating(false);
		this.InitialiseInputsAndLayers();
		while(!initialised) // Wait for layers to be initialised before running through lines of inputs
		{
			Log.addToLog("Waiting for ANN "+this.populationMemberID+" to initialise");
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.startRunningThroughEachLineOfData();
	}

	public void InitialiseInputsAndLayers()
	{

		if(this.firstLayer.size() == 0) // If the first evolution, go ahead and initialise. 
		{
			this.initialiseFirstLayerNodes();
		}
		if(this.hiddenLayers.size() == 0) // If no hidden layers have been intialised yet, do it
		{
			this.initialiseHiddenLayerNodes();
		}
		if(outputNode == null)// First evolution, so go ahead and initialse
		{
			this.initialiseOutputNode();
		}
	}

	public void initialiseFirstLayerNodes()
	{
		this.firstLayer.clear();
		Log.addToLog("--------Node "+this.populationMemberID+" Initialising First Layer of Nodes------");
		// Create the number of first-layer nodes based on the number of inputs. 
		for(int layerNode = 0; layerNode < this.numFirstLayer; layerNode++)
		{
			Node newTemp = new Node(layerNode,this.activationFunction); // This is a node in the first layer

			for(Integer input = 0; input < this.numInputs; input++) // Want to add edges from each input to each first layer node. 
			{
				Double tempWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
				//System.out.println("Setting weight for incoming edge # "+input+" for node "+layerNode+" between -5 and +5 as "+this.decimalFormat.format(tempWeight));

				IncomingEdge tempEdge = new IncomingEdge();
				tempEdge.weightValue = tempWeight;
				newTemp.getIncomingEdgeList().add(tempEdge);
			}
			// Now add the Bias value and weight
			Double tempBiasWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
			Double tempBiasValue = 1.0;
			newTemp.setBiasWeight(tempBiasWeight);
			newTemp.setBiasValue(tempBiasValue);

			// Now add the node to the first layer
			firstLayer.add(newTemp);
		}

		//System.out.println("You have created "+firstLayer.size()+" first layer nodes with inputs of:");
		for( Node n : firstLayer)
		{
			//System.out.println("Node "+n.getNodeNumber()+ " has incoming edges: ");

			for(IncomingEdge edge : n.getIncomingEdgeList())
			{
				//System.out.println("\tEdge"+edge.getKey()+ ": has weight of "+edge.getValue().weightValue);
			}
			//System.out.println("\tNode "+n.getNodeNumber()+ " has bias value of "+this.decimalFormat.format(n.getBiasValue())+" and bias weight "+this.decimalFormat.format(n.getBiasWeight()));
		}
		//System.out.println("---- Finished Initialising First Layer of Nodes-----");
		// They were created through crossover
	}

	public void initialiseHiddenLayerNodes()
	{
		this.hiddenLayers.clear();
		Log.addToLog("hidden layers was empty for ann "+this.populationMemberID+", populating");
		for(int x = 0; x < this.numHiddenLayers; x++)
		{
			this.hiddenLayers.add(new ArrayList<Node>());
		}

		for(int x = 0; x < this.hiddenLayers.size(); x++)
		{

			Log.addToLog("--------Node "+this.populationMemberID+" Initialising Hidden Layer #"+x+" of Nodes------");

			System.out.println("num nodes in hidden layer "+x+" is "+this.getNumInEachHiddenLayer().get(x));
			// Create the number of hidden-layer nodes as 5 because the coursework said to do this. 
			for(int layerNode = 0; layerNode < this.getNumInEachHiddenLayer().get(x); layerNode++)
			{
				Node newTemp = new Node(layerNode,this.activationFunction);

				int depends = 0;
				// Set depends to be number of nodes in previous layer. 
				if(x == 0)
				{ // If x is 0 then previous layer is the First layer. 
					depends = this.numFirstLayer;
				}
				else
				{
					// If x is not 0 then the previous layer is the previous Hidden layer
					depends = this.hiddenLayers.get(x-1).size();
				}
				for(int input = 0; input < depends; input++) // Want to add edges from each first layer node. 
				{
					Double tempWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
					//System.out.println("Setting weight for incoming edge # "+input+" for node "+layerNode+" between -5 and +5 as "+this.decimalFormat.format(tempWeight));

					IncomingEdge tempEdge = new IncomingEdge();
					tempEdge.weightValue = tempWeight;
					newTemp.getIncomingEdgeList().add(tempEdge);
				}

				// Now add the Bias value and weight
				Double tempBiasWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
				Double tempBiasValue = 1.0;
				newTemp.setBiasWeight(tempBiasWeight);
				newTemp.setBiasValue(tempBiasValue);

				// Now add this node to the hidden layer. 
				this.hiddenLayers.get(x).add(newTemp);
			}

			System.out.println("You have created "+hiddenLayers.get(x).size()+" hidden layer nodes with inputs of:");
			for( Node n : this.hiddenLayers.get(x))
			{
				//System.out.println("Node "+n.getNodeNumber()+ " has incoming edges: ");

				for(IncomingEdge edge : n.getIncomingEdgeList())
				{
					//System.out.println("\tEdge"+edge+ ": has weight of "+edge.weightValue);
				}
				//System.out.println("\tNode "+n.getNodeNumber()+ " has bias value of "+this.decimalFormat.format(n.getBiasValue())+" and bias weight "+this.decimalFormat.format(n.getBiasWeight()));
			}
			System.out.println("---- Finished Initialising Hidden Layer of Nodes-----");
		}
	}

	public void initialiseOutputNode()
	{
		Log.addToLog("--------Node "+this.populationMemberID+" Initialising Output Node------");
		this.outputNode = new Node(0,this.activationFunction);
		for(Integer layerNode = 0; layerNode < (this.hiddenLayers.get(hiddenLayers.size()-1).size()); layerNode++) // Want to add edges from each of the last hidden layer nodes. 
		{
			Double tempWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
			//System.out.println("Setting weight for incoming edge # "+layerNode+" for the output node between -5 and +5 as "+this.decimalFormat.format(tempWeight));

			IncomingEdge tempEdge = new IncomingEdge();
			tempEdge.weightValue = tempWeight;
			this.outputNode.getIncomingEdgeList().add(tempEdge);
		}
		// Now add the Bias value and weight for the single output node
		Double tempBiasWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
		Double tempBiasValue = 1.0;
		this.outputNode.setBiasWeight(tempBiasWeight);
		this.outputNode.setBiasValue(tempBiasValue);

		//System.out.println("Output Node "+this.outputNode.getNodeNumber()+ " has incoming edges: ");

		for(IncomingEdge edge : this.outputNode.getIncomingEdgeList())
		{
			//System.out.println("\tEdge"+ ": has weight of "+edge.weightValue);
		}
		//System.out.println("\tOutput Node "+this.outputNode.getNodeNumber()+ " has bias value of "+this.decimalFormat.format(this.outputNode.getBiasValue())+" and bias weight "+this.decimalFormat.format(this.outputNode.getBiasWeight()));

		//	System.out.println("---- Finished Initialising Output Node-----");
		this.initialised = true;
	}

	public void startRunningThroughEachLineOfData()
	{
		// Clear out the last run's  data. 
		this.actualOutputs.clear();
		this.expectedOutputs.clear();
		this.differenceBetweenEandO.clear();
		Log.addToLog("Node "+this.populationMemberID+" starting running through the lines of data.");
		ArrayList<Double[]> functionData = Inputs.getFunctionsData().get(dataholder.getFunctionToApprox());
		for(Double[] entry : functionData) // Cycle through each line of data and run the MLP for each input set of data. 
		{	
			// Cycle through each first-layer node and apply each input value. 
			for(Node firstLayerNode : this.firstLayer) 
			{
				// Cycle through each input in the line of data, minus 1 because don't want to include the last value (the output we are hoping to achieve).
				int index = 0;
				here:for(IncomingEdge ie : firstLayerNode.getIncomingEdgeList())
				{
					ie.inputValue = entry[index];
					index++;
					if(index == entry.length -1)
					{
						break here;
					}
				}
			}

			// Set the expected output value
			this.expectedOutputValue = entry[entry.length-1];// Get the last entry value which will be the expected output (the y value).

			// Check everything is ok. 
			/*for(Node n : this.firstLayer)
			{
				Log.addToLog("ANN "+this.populationMemberID+" Node "+n.getNodeNumber()+" has been given starting inputs of: ");
				System.out.println("Node number "+n.getNodeNumber()+": ");
				for(IncomingEdge entryEdge : n.getIncomingEdgeList())
				{
					Log.addToLog("\tEdge "+entryEdge.getKey()+" has inputs of:");
					Log.addToLog("\t\tWeight: "+decimalFormat.format(entryEdge.getValue().weightValue)+", Input value: "+decimalFormat.format(entryEdge.getValue().inputValue));
				}
			}*/
			// Now fire the activation function and determine output for each first-layer node. 
			for(Node n : this.firstLayer)
			{
				Double result = n.getActivationFunction().tryActivation(n.getIncomingEdgeList(),n.getBiasWeight(),n.getBiasValue());
				n.setOutputValue(result);
				//Log.addToLog("First-layer Node "+n.getNodeNumber()+" has been activated and has output : "+result);
			}

			// Now feed into the hidden layer
			for(int z = 0; z < this.hiddenLayers.size(); z++)
			{
				for(Node hidden : this.hiddenLayers.get(z))//cycle through all hidden layer nodes
				{
					ArrayList<IncomingEdge> tempmap = hidden.getIncomingEdgeList();
					if(z == 0)
					{ // If the first hidden layer then get inputs from First layer
						int incrementer = 0;
						for(IncomingEdge ie : tempmap)
						{
							ie.inputValue = this.firstLayer.get(incrementer).getOutputValue();
							incrementer++;
						}
					}
					else //If not first hidden layer then get inputs from prev hidden layer
					{
						int incrementer = 0;
						for(IncomingEdge ie : tempmap)
						{
							ie.inputValue = this.hiddenLayers.get(z-1).get(incrementer).getOutputValue();
							incrementer++;
						}
					}

					// Now try the activation function
					hidden.setOutputValue(hidden.getActivationFunction().tryActivation(hidden.getIncomingEdgeList(), hidden.getBiasWeight(), hidden.getBiasValue()));
					//Log.addToLog("Hidden node "+hidden.getNodeNumber()+" has been activated and has output "+hidden.getOutputValue());
				}
			}

			// Now feed into the output node

			ArrayList<IncomingEdge> tempmap = outputNode.getIncomingEdgeList();
			int incrementer = 0;
			
			//System.out.println("last hidden layer has "+this.hiddenLayers.get(hiddenLayers.size()-1).size()+" and output edges are size "+outputNode.getIncomingEdgeList().size());
			
			for(IncomingEdge ie : tempmap)
			{
				ie.inputValue = this.hiddenLayers.get(hiddenLayers.size()-1).get(incrementer).getOutputValue();
				incrementer++;
			}

			// Now try the activation function
			//System.out.println("trying activation for ANN "+this.populationMemberID);
			this.outputNode.setOutputValue(outputNode.getActivationFunction().tryActivation(outputNode.getIncomingEdgeList(), outputNode.getBiasWeight(), outputNode.getBiasValue()));
			//Log.addToLog("ANN "+this.populationMemberID+", Output node "+outputNode.getNodeNumber()+" has been activated and has output "+outputNode.getOutputValue() + " but expected output is "+this.expectedOutputValue);

			// Don't want the output to go outside the range being approximated, 
			// so setting a ceiling output of 1 and floor of -1
			/*if(outputNode.getOutputValue() > 1 )
			{
				this.outputNode.setBiasValue(outputNode.getBiasValue()-0.1 );
			}
			else if(outputNode.getOutputValue() < -1)
			{
				this.outputNode.setBiasValue(outputNode.getBiasValue()+0.1 );
			}*/


			// Here populating actual output values, which are later used to draw the graph
			//System.out.println(this.populationMemberID+" output value from output node is "+outputNode.getOutputValue());
			this.actualOutputs.add(outputNode.getOutputValue());
			this.expectedOutputs.add(this.expectedOutputValue);

			// This is used to calculate LMS later
			this.differenceBetweenEandO.add(this.expectedOutputValue - outputNode.getOutputValue());

			//Fitness currentFitness = new Fitness(this.expectedOutputValue,outputNode.getOutputValue());
			//System.out.println("Node "+this.populationMemberID+" Current fitness is "+currentFitnessValue);
			//this.FitnessList.add(currentFitness);

			//Log.addToLog("Line read for ANN "+this.populationMemberID+" finished with fitness of "+currentFitnessValue.toString()+" where expected output was "+this.expectedOutputValue+" and actual output was "+outputNode.getOutputValue());
		}

		Double overallfitness = this.CalculateFitness();
		this.setOverallFitness(overallfitness);
		this.setOverallLMS(this.calculateLMS());
		dataholder.addToCurrentEvolutionOverallFitnessValues(this.OverallFitness);
		System.out.println(this.populationMemberID+" overall fitness has been set as "+this.getOverallFitness()+" and lms "+this.getOverallLMS());
		Log.addToLog("Overall fitness for ANN "+this.populationMemberID+" is "+this.OverallFitness);
		Log.addJustOverallFitness(dataholder.getCurrentEvolutionNumber(),this.OverallFitness);


		// Wait for calculate fitness method to finish, this was causing problems as the next section of code was being run before calculate fitness had completed. 
		while(this.getOverallFitness() == null)
		{
			try {
				System.out.println("waiting for "+this.populationMemberID+" to calculate and set fitness");
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(dataholder.getFittestEver() != null)
		{
			// Smaller fitness values are better, 0 is a perfect score. 
			if(dataholder.getFittestEver().getOverallFitness() > this.getOverallFitness())
			{
				NeuralNetwork fittest = new NeuralNetwork(dataholder,this.getActivationFunction(),this.populationMemberID,dataholder.getWeightMinimum(),dataholder.getWeightMaximum());
				fittest = this.initialiseFittest(fittest, this);
				//fittest.FitnessList = this.FitnessList;
				fittest.setOverallFitness(this.getOverallFitness());

				fittest.setOverallLMS(fittest.calculateLMS());
				dataholder.setFittestEver(fittest);
				//System.out.println("fittest lms is"+fittest.overallLMS);
			}
			else
			{
				// Do nothing as this ANN is not fitter than the fittest. 
			}
		}
		else
		{
			System.out.println("dataholder fittest ever was null, setting one now");

			NeuralNetwork fittest = new NeuralNetwork(dataholder,this.getActivationFunction(),this.populationMemberID,dataholder.getWeightMinimum(),dataholder.getWeightMaximum());

			fittest = this.initialiseFittest(fittest, this);

			//fittest.FitnessList = this.FitnessList;
			fittest.setOverallFitness(this.getOverallFitness());

			fittest.overallLMS = fittest.calculateLMS();
			//System.out.println("fittest overalllms is "+fittest.overallLMS);
			dataholder.setFittestEver(fittest);
		}

		Log.addToLog("Ok finished running through data and also calculating overall fitness of ANN "+this.populationMemberID);
	}

	private NeuralNetwork initialiseFittest(NeuralNetwork fittest,NeuralNetwork parent1)
	{
		//Initialise first layer of child from parent1
		fittest.setEvolutionCycle(dataholder.getCurrentEvolutionNumber());// Used in the end of the log. 
		fittest.setActualOutputs(parent1.getActualOutputs());
		fittest.setExpectedOutputs(parent1.getExpectedOutputs());
		fittest.setDifferenceBetweenEandO(parent1.getDifferenceBetweenEandO());

		fittest.setNumFirstLayer(parent1.getNumFirstLayer());
		fittest.initialiseFirstLayerNodes();
		fittest.setNumInEachHiddenLayer(parent1.getNumbersInHiddenLayers());
		fittest.setNumHiddenLayers(parent1.getNumHiddenLayers());
		fittest.initialiseHiddenLayerNodes();
		fittest.initialiseOutputNode();

		// Now copy over the weights from parent1 to child
		for(int x=0; x < fittest.getNumFirstLayer(); x++)
		{
			fittest.getFirstLayerNodes().get(x).setBiasValue(parent1.getFirstLayerNodes().get(x).getBiasValue());
			fittest.getFirstLayerNodes().get(x).setBiasWeight( parent1.getFirstLayerNodes().get(x).getBiasWeight() );
			for(int y=0; y < fittest.getFirstLayerNodes().get(x).getIncomingEdgeList().size(); y++ )
			{
				fittest.getFirstLayerNodes().get(x).getIncomingEdgeList().get(y).weightValue = 
						parent1.getFirstLayerNodes().get(x).getIncomingEdgeList().get(y).weightValue;
			}
		}

		for(int z=0; z < fittest.getNumHiddenLayers(); z++)
		{
			for(int x=0; x < fittest.getHiddenLayers().get(z).size(); x++)
			{
				fittest.getHiddenLayers().get(z).get(x).setBiasValue(parent1.getHiddenLayers().get(z).get(x).getBiasValue());
				fittest.getHiddenLayers().get(z).get(x).setBiasWeight( parent1.getHiddenLayers().get(z).get(x).getBiasWeight() );

				for(int y=0; y < fittest.getHiddenLayers().get(z).get(x).getIncomingEdgeList().size(); y++ )
				{
					fittest.getHiddenLayers().get(z).get(x).getIncomingEdgeList().get(y).weightValue = 
							parent1.getHiddenLayers().get(z).get(x).getIncomingEdgeList().get(y).weightValue;
				}
			}
		}

		fittest.getOutputLayerNode().setBiasValue(parent1.getOutputLayerNode().getBiasValue());
		fittest.getOutputLayerNode().setBiasWeight(parent1.getOutputLayerNode().getBiasWeight());
		for(int y=0; y < fittest.getOutputLayerNode().getIncomingEdgeList().size(); y++ )
		{
			fittest.getOutputLayerNode().getIncomingEdgeList().get(y).weightValue = 
					parent1.getOutputLayerNode().getIncomingEdgeList().get(y).weightValue;
		}

		return fittest;
	}

	public Double calculateLMS()
	{
		Double returner = 0.0;
		//System.out.println("difference is size "+this.differenceBetweenEandO.size());
		for(Double d : this.differenceBetweenEandO)
		{
			//System.out.println("difference is "+d);
			// Sum the square of the differences between expected and actual
			returner += Math.abs(d*d);
		}

		return returner;
	}

	public Double CalculateFitness()
	{
		Double summer = 0.0;
		for(Double d : this.differenceBetweenEandO)
		{
			//System.out.println("difference is "+d);
			// Sum the square of the differences between expected and actual
			summer += Math.abs(d*d);
		}

		return summer;
	}

	public Boolean mutateWeightsWithProbability()
	{
		boolean happens = random.nextDouble() < (dataholder.getMutationRate());//http://stackoverflow.com/questions/10368202/java-how-do-i-simulate-probability
		if(happens) // do mutation
		{
			double chance = Math.random();
			if(chance < 0.7) // 70% chance of mutating single node in each layer, less disruptive so i want this mostly
			{
				Random ran = new Random();
				// Mutate a first-layer node
				int whichFirstLayerNode = ran.nextInt(firstLayer.size());

				Node theChosenFirstLayerNode = firstLayer.get(whichFirstLayerNode);

				for(IncomingEdge incomingEdge : theChosenFirstLayerNode.getIncomingEdgeList())
				{Double changer = Math.random();
				changer = changer / 6; // Dividing by six because I want a small impact, need to evolve slowly toward the optimum. 
				Double x = Math.random();
				if(x < 0.5)
				{
					//Ensure within weight min/max
					if((incomingEdge.weightValue - changer) >= this.weightMinimum)
					{
						Log.addToLog("ANN "+this.populationMemberID+", Firstlayer Node "+theChosenFirstLayerNode.getNodeNumber()+" Decrementing weight by "+changer+" for edge "+incomingEdge.toString());
						incomingEdge.weightValue -= changer; // Decrement weight by a fraction of 1
						theChosenFirstLayerNode.setBiasWeight(theChosenFirstLayerNode.getBiasWeight()-changer);
					}
				}
				else
				{
					//Ensure within weight min/max
					if((incomingEdge.weightValue + changer) <= this.weightMaximum)
					{
						Log.addToLog("ANN "+this.populationMemberID+", Firstlayer Node "+theChosenFirstLayerNode.getNodeNumber()+" Incrementing weight by "+changer+" for edge "+incomingEdge.toString());
						incomingEdge.weightValue += changer; // Increment weight by a fraction of 1
						theChosenFirstLayerNode.setBiasWeight(theChosenFirstLayerNode.getBiasWeight()+changer);
					}
				}
				}

				// Mutate a hidden-layer node
				for(int y = 0; y < hiddenLayers.size(); y++)
				{
					int whichHiddenLayerNode = ran.nextInt(hiddenLayers.get(y).size());

					Node theChosenHiddenLayerNode = hiddenLayers.get(y).get(whichHiddenLayerNode);
					for(IncomingEdge incomingEdge : theChosenHiddenLayerNode.getIncomingEdgeList())
					{
						Double changer = Math.random();
						changer = changer / 6;
						Double x = Math.random(); // x has a 50/50 chance of being 0.0->0.4 or 0.5->0.9
						if(x < 0.5)
						{
							//Ensure within weight min/max
							if((incomingEdge.weightValue - changer) >= this.weightMinimum)
							{
								Log.addToLog("ANN "+this.populationMemberID+", Hiddenlayer Node "+theChosenHiddenLayerNode.getNodeNumber()+" Decrementing weight by "+changer+" for edge "+incomingEdge.toString());
								incomingEdge.weightValue -= changer; // Decrement weight by a fraction of 1
								theChosenHiddenLayerNode.setBiasWeight(theChosenHiddenLayerNode.getBiasWeight()-changer);
							}
						}
						else
						{
							//Ensure within weight min/max
							if((incomingEdge.weightValue + changer) <= this.weightMaximum)
							{
								Log.addToLog("ANN "+this.populationMemberID+", Hiddenlayer Node "+theChosenHiddenLayerNode.getNodeNumber()+" Incrementing weight by "+changer+" for edge "+incomingEdge.toString());
								incomingEdge.weightValue += changer; // Increment weight by a fraction of 1
								theChosenHiddenLayerNode.setBiasWeight(theChosenHiddenLayerNode.getBiasWeight()+changer);
							}
						}
					}
				}


				// Mutate the output node with a 50/50 chance
				Double maybe = Math.random();
				if(maybe < 0.5)
				{
					for(IncomingEdge incomingEdge : this.outputNode.getIncomingEdgeList())
					{
						Double changer = Math.random();
						changer = changer / 6;
						Double x = Math.random(); // x has a 50/50 chance of being 0.0->0.4 or 0.5->0.9
						if(x < 0.5)
						{
							//Ensure within weight min/max
							if((incomingEdge.weightValue - changer) >= this.weightMinimum)
							{
								Log.addToLog("ANN "+this.populationMemberID+", Outputlayer Node "+outputNode.getNodeNumber()+" Decrementing weight by "+changer+" for edge "+incomingEdge.toString());
								incomingEdge.weightValue -= changer; // Decrement weight by a fraction of 1
								outputNode.setBiasWeight(outputNode.getBiasWeight()-changer);
							}
						}
						else
						{
							//Ensure within weight min/max
							if((incomingEdge.weightValue + changer) <= this.weightMaximum)
							{
								Log.addToLog("ANN "+this.populationMemberID+", output Node Incrementing weight by "+changer+" for edge "+incomingEdge.toString());
								incomingEdge.weightValue += changer; // Increment weight by a fraction of 1
								outputNode.setBiasWeight(outputNode.getBiasWeight()+changer);
							}
						}
					}
				}
				else
				{} // Don't mutate the output node. 

			}
			else // 30% chance of mutating all the nodes in each layer, more disruptive so want only 30% chance of it
			{
				Log.addToLog("In mutate for ANN "+this.populationMemberID+" size of firstlayer is "+firstLayer.size());
				for(Node firstLayerNode : this.firstLayer)
				{
					for(IncomingEdge incomingEdge: firstLayerNode.getIncomingEdgeList())
					{
						Double changer = Math.random();
						changer = changer / 6;
						Double x = Math.random(); // x has a 50/50 chance of being 0.0->0.4 or 0.5->0.9
						if(x < 0.5)
						{
							//Ensure within weight min/max
							if((incomingEdge.weightValue - changer) >= this.weightMinimum)
							{
								Log.addToLog("ANN "+this.populationMemberID+", Firstlayer Node "+firstLayerNode.getNodeNumber()+" Decrementing weight by "+changer+" for edge "+incomingEdge.toString());
								incomingEdge.weightValue -= changer; // Decrement weight by a fraction of 1
							}
						}
						else
						{
							//Ensure within weight min/max
							if((incomingEdge.weightValue + changer) <= this.weightMaximum)
							{
								Log.addToLog("ANN "+this.populationMemberID+", Firstlayer Node "+firstLayerNode.getNodeNumber()+" Incrementing weight by "+changer+" for edge "+incomingEdge.toString());
								incomingEdge.weightValue += changer; // Increment weight by a fraction of 1
							}
						}
					}
				}
				Log.addToLog("In mutate for ANN "+this.populationMemberID+" size of hiddenlayer is "+hiddenLayer.size());
				for(Node hiddenLayerNode : this.hiddenLayer)
				{
					for(IncomingEdge incomingEdge: hiddenLayerNode.getIncomingEdgeList())
					{
						Double changer = Math.random();
						changer = changer / 6;
						Double x = Math.random(); // x has a 50/50 chance of being 0.0->0.4 or 0.5->0.9
						if(x < 0.5)
						{
							Log.addToLog("ANN "+this.populationMemberID+", Hiddenlayer Node "+hiddenLayerNode.getNodeNumber()+" Decrementing weight by "+changer+" for edge "+incomingEdge.toString());
							incomingEdge.weightValue -= changer; // Decrement weight by a fraction of 1
						}
						else
						{
							Log.addToLog("ANN "+this.populationMemberID+", Hiddenlayer Node "+hiddenLayerNode.getNodeNumber()+" Incrementing weight by "+changer+" for edge "+incomingEdge.toString());
							incomingEdge.weightValue += changer; // Increment weight by a fraction of 1
						}
					}
				}

				for(IncomingEdge incomingEdge: outputNode.getIncomingEdgeList())
				{
					Double changer = Math.random();
					changer = changer / 6;
					Double x = Math.random(); // x has a 50/50 chance of being 0.0->0.4 or 0.5->0.9
					if(x < 0.5)
					{
						Log.addToLog("ANN "+this.populationMemberID+", Output Node "+outputNode.getNodeNumber()+" Decrementing weight by "+changer+" for incoming edge"+incomingEdge.toString());
						incomingEdge.weightValue -= changer; // Decrement weight by a fraction of 1
					}
					else
					{
						Log.addToLog("ANN "+this.populationMemberID+", Output Node "+outputNode.getNodeNumber()+" Incrementing weight by "+changer+" for incoming edge"+incomingEdge.toString());
						incomingEdge.weightValue += changer; // Increment weight by a fraction of 1
					}
				}
			}

			return true;
		}
		return false;
	}

	public void mutateActivationWithProbability()
	{
		boolean happens = random.nextDouble() < (dataholder.getMutationRate());//http://stackoverflow.com/questions/10368202/java-how-do-i-simulate-probability
		if(happens) // do mutation of Activation Function. 
		{
			Log.addToLog("Mutating activation function of a single node in each layer for ann "+this.populationMemberID);
			// Mutate a single first-layer node
			int whichFirstLayerNode = random.nextInt(firstLayer.size());
			firstLayer.get(whichFirstLayerNode).setActivationFunction(chooseActivationFunctionByEqualChance());

			//Mutate a single hidden layer node
			for(int x = 0; x < hiddenLayers.size(); x++)
			{
				int whichHiddenLayerNode = random.nextInt(hiddenLayers.get(x).size());
				hiddenLayers.get(x).get(whichHiddenLayerNode).setActivationFunction(chooseActivationFunctionByEqualChance());

			}
			// Mutate output node with 50/50 chance
			Double chance = Math.random();
			if(chance < 0.5)
			{
				outputNode.setActivationFunction(chooseActivationFunctionByEqualChance());
			}
			else{} // Don't mutate output node activation function	
		}
	}

	public void mutateTopologyWithProbability()
	{
		boolean happens = random.nextDouble() < (dataholder.getMutationRate());//http://stackoverflow.com/questions/10368202/java-how-do-i-simulate-probability
		if(happens) // do mutation of Topology (num nodes in first layer, num in hidden and number of hidden layers). 
		{
			// First mutate the first layer by adding or removing a node there
			this.mutateAddorRemoveNodeInFirstLayer();

			//Next add or remove a node in each hidden layer
			this.mutateAddorRemoveNodeInHiddenLayers();

			//Next mutate the number of hidden layers by incrementing or decrementing
			this.mutateHowManyHiddenLayers();
		}

		this.setFinishedMutating(true);
	}

	private void mutateAddorRemoveNodeInFirstLayer()
	{
		double coin = Math.random();
		if(coin < 0.5)
		{
			// Increment nodes up to 10 max
			if(this.firstLayer.size() == 10)
			{
				// HIt the max size of 10
				Log.addToLog("Attempted mutating first layer of "+this.populationMemberID+" but already reached max of 10 nodes in first layer");
			}
			else
			{
				Node newFirst = new Node(this.firstLayer.size(),this.activationFunction);

				for(Integer input = 0; input < this.numInputs; input++) // Want to add edges from each input to each first layer node. 
				{
					Double tempWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
					//System.out.println("Setting weight for incoming edge # "+input+" for node "+layerNode+" between -5 and +5 as "+this.decimalFormat.format(tempWeight));

					IncomingEdge tempEdge = new IncomingEdge();
					tempEdge.weightValue = tempWeight;
					newFirst.getIncomingEdgeList().add(tempEdge);
				}
				// Now add the Bias value and weight
				Double tempBiasWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
				Double tempBiasValue = 1.0;
				newFirst.setBiasWeight(tempBiasWeight);
				newFirst.setBiasValue(tempBiasValue);

				// Now add the node to the first layer
				Log.addToLog("size of first layer before topology mutate is "+firstLayer.size());
				firstLayer.add(newFirst);
				Log.addToLog("size of first layer after adding node is "+firstLayer.size());
				this.numFirstLayer++;

				// Now update hidden layer by adding an extra IncomingEdge with random weight for each node. 
				for(Node n : hiddenLayers.get(0))
				{
					IncomingEdge ie = new IncomingEdge();
					ie.weightValue = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
					n.getIncomingEdgeList().add(ie);
				}
				Log.addToLog("mutating first layer by adding an extra node for ANN "+this.populationMemberID);
			}
		}
		else // Decrement nodes down to 1 minimum
		{
			if(this.firstLayer.size() < 2)
			{
				// HIt the min size of 1
				Log.addToLog("Attempted mutating first layer of "+this.populationMemberID+" but already reached min of 1 nodes in first layer");
			}
			else
			{
				// Remove the first node in the firstlayer
				firstLayer.remove(0);
				this.numFirstLayer--;

				// Now update the first hidden layer
				// Need to remove the first IncomingEdge from each node
				for(Node n : hiddenLayers.get(0))
				{
					System.out.println(n.getIncomingEdgeList().size()+" is the size of incoming edge list before");
					n.getIncomingEdgeList().remove(0);
					System.out.println(n.getIncomingEdgeList().size()+" is the size of incoming edge list after");

				}
				Log.addToLog("mutating first layer by removing a node for ANN "+this.populationMemberID);
			}
		}
	}

	private void mutateAddorRemoveNodeInHiddenLayers()
	{
		for(int z = 0; z < hiddenLayers.size(); z++) // Loop through hidden layers
		{
			Double incOrDec = Math.random();
			if(incOrDec < 0.5)
			{//increment

				// Check the ceiling
				if(hiddenLayers.get(z).size() > 9)
				{
					// Don't bother if ceiling of 10 has been reached. 
				}
				else // There's still space for more
				{
					Node hiddennode = new Node(hiddenLayers.get(z).size(),this.activationFunction);
					// Set node's bias weight and value. 
					Double tempBiasWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
					Double tempBiasValue = 1.0;
					hiddennode.setBiasWeight(tempBiasWeight);
					hiddennode.setBiasValue(tempBiasValue);
					// Initialise the node with an IncomingEdge list

					if(z == 0)// previous layer is first layer
					{
						for(Node n : firstLayer)
						{// Add an incoming edge for each previous layer node
							IncomingEdge newEdge = new IncomingEdge();
							newEdge.weightValue = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
							hiddennode.getIncomingEdgeList().add(newEdge);
						}
					}
					else // Previous layer is a hidden layer
					{
						for(Node n : hiddenLayers.get(z-1))
						{// Add an incoming edge for each previous layer node
							IncomingEdge newEdge = new IncomingEdge();
							newEdge.weightValue = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
							hiddennode.getIncomingEdgeList().add(newEdge);
						}
					}
					Log.addToLog("size of hidden layer "+z+" before adding node is "+hiddenLayers.get(z).size());
					hiddenLayers.get(z).add(hiddennode);
					Log.addToLog("size of hidden layer "+z+" after adding node is "+hiddenLayers.get(z).size());
					// Now need to add to the following layer's incoming edges because there is a new one now

					// If this is final hidden layer, need to update output node
					if(z == hiddenLayers.size()-1)
					{
						IncomingEdge newEdge = new IncomingEdge();
						newEdge.weightValue = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
						this.outputNode.getIncomingEdgeList().add(newEdge);
					}
					// Else if this is not the final hidden layer, need to add new edge to each node in the next hidden layer
					else
					{
						for(Node n : hiddenLayers.get(z+1))
						{// Add an incoming edge for each previous layer node
							IncomingEdge newEdge = new IncomingEdge();
							newEdge.weightValue = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
							n.getIncomingEdgeList().add(newEdge);
						}
					}
				}
				Log.addToLog("Just added a new node to hidden layer "+z+" in ANN "+this.populationMemberID);
			}
			else
			{// decrement

				//Check that floor of 1 has not been reached. 

				if(hiddenLayers.get(z).size() < 2)
				{
					// Don't bother
				}
				else
				{
					Log.addToLog("size of hidden layer "+z+" before removing a node is "+hiddenLayers.get(z).size());
					hiddenLayers.get(z).remove(0);
					Log.addToLog("size of hidden layer "+z+" after removing a node is "+hiddenLayers.get(z).size());

					// Now remove an incoming edge from the subsequent layer's nodes
					if(z == hiddenLayers.size()-1)
					{
						this.outputNode.getIncomingEdgeList().remove(0);
					}
					// Else if this is not the final hidden layer, need to remove an edge for each node in the next hidden layer
					else
					{
						for(Node n : hiddenLayers.get(z+1))
						{// Add an incoming edge for each previous layer node
							n.getIncomingEdgeList().remove(0);
						}
					}
				}
				Log.addToLog("Just removed a node from hidden layer "+z+" in ANN "+this.populationMemberID);
			}
		}
	}

	private void mutateHowManyHiddenLayers()
	{
		Double coin = Math.random();
		if(coin < 0.5) // Increment by one
		{
			// Check that ceiling not hit
			if(hiddenLayers.size() > 9)
			{
				// Don't bother. 
			}
			else
			{
				// Add to this the array which holds the node totals for each hidden layer, put the same num of nodes as the previous hidden layer had. 
				this.numInEachHiddenLayer.add(this.numInEachHiddenLayer.get(numInEachHiddenLayer.size()-1)); 
				ArrayList<Node> newHiddenLayer = new ArrayList<Node>();
				hiddenLayers.add(newHiddenLayer);

				int x = hiddenLayers.size()-1; // The index of the latest additional hidden-layer. 

				Log.addToLog("--------Node "+this.populationMemberID+" Mutation by Initialising New Hidden Layer #"+x+" of Nodes------");

				System.out.println("num nodes in new mutatedly added hidden layer "+x+" will be "+this.getNumInEachHiddenLayer().get(x));
				 
				for(int layerNode = 0; layerNode < this.getNumInEachHiddenLayer().get(x); layerNode++)
				{
					Node newTemp = new Node(layerNode,this.activationFunction);

					int depends = 0;

					// If x is not 0 then the previous layer is the previous Hidden layer
					depends = this.hiddenLayers.get(x-1).size();

					for(int input = 0; input < depends; input++) // Want to add edges for each previous layer node. 
					{
						Double tempWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
						//System.out.println("Setting weight for incoming edge # "+input+" for node "+layerNode+" between -5 and +5 as "+this.decimalFormat.format(tempWeight));

						IncomingEdge tempEdge = new IncomingEdge();
						tempEdge.weightValue = tempWeight;
						newTemp.getIncomingEdgeList().add(tempEdge);
					}

					// Now add the Bias value and weight
					Double tempBiasWeight = ThreadLocalRandom.current().nextDouble(this.weightMinimum, this.weightMaximum);
					Double tempBiasValue = 1.0;
					newTemp.setBiasWeight(tempBiasWeight);
					newTemp.setBiasValue(tempBiasValue);

					// Now add this node to the hidden layer. 
					this.hiddenLayers.get(x).add(newTemp);
				}

				System.out.println("You have created "+hiddenLayers.get(x).size()+" hidden layer nodes with inputs of:");
				for( Node n : this.hiddenLayers.get(x))
				{
					System.out.println("Node "+n.getNodeNumber()+ " has incoming edges: ");

					for(IncomingEdge edge : n.getIncomingEdgeList())
					{
						System.out.println("\tEdge"+edge+ ": has weight of "+edge.weightValue);
					}
					System.out.println("\tNode "+n.getNodeNumber()+ " has bias value of "+this.decimalFormat.format(n.getBiasValue())+" and bias weight "+this.decimalFormat.format(n.getBiasWeight()));
				}
				System.out.println("---- Finished Mutating Hidden Layer of Nodes-----");
				// No need to update output node because the new hidden layer has the same number of nodes as the previous one
				this.numHiddenLayers++; // Increment the value of how many hidden layers there are, this is used by, for example, breeding. 
				
				/// I will update the outputnode incomingedges because i was getting problems with them.... 
				this.initialiseOutputNode(); // Creates a new one. 
			}
		}
		else // Decrement by one. 
		{

		}
	}

	private ActivationFunctionParent chooseActivationFunctionByEqualChance()
	{
		ActivationFunctionParent returner;
		double spinner = Math.random();
		if(spinner < 0.2)
		{
			returner = new AFnull();
		}
		else if(spinner >= 0.2 && spinner < 0.4)
		{
			returner = new AFcosine();
		}
		else if(spinner >= 0.4 && spinner < 0.6)
		{
			returner = new AFhyperbolicTangent();
		}
		else if(spinner >= 0.6 && spinner < 0.8)
		{
			returner = new AFsigmoid();
		}
		else
		{
			returner = new AFgaussian();
		}
		return returner;
	}

	public Boolean isAlive()
	{
		if(Thread.currentThread().isAlive())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
