package Model;

import java.util.*;

import Evolvers.ActivationFunctionParent;

public class Node
{
	private int nodeNumber;
	private double inputWeight;
	private double inputBiasWeight;
	private double inputBiasValue;
	private double outputValue;
	private ArrayList<IncomingEdge> incomingEdgeList = new ArrayList<IncomingEdge>(); // Integer is edge number, IncomingEdge is to hold two values, first is weight value, second is input value. 
	private ActivationFunctionParent activationFunction;

	public synchronized ActivationFunctionParent getActivationFunction() {
		return activationFunction;
	}
	
	public void setActivationFunction(ActivationFunctionParent genericActivationFunction)
	{
		this.activationFunction = genericActivationFunction;
	}

	public Node(int number, ActivationFunctionParent afp)
	{
		this.nodeNumber = number;
		this.activationFunction = afp;
	}

	public Boolean activate()
	{
		Boolean returner = false;
		//If activate successfull return true else return false
		return returner;
	}

	public int getNodeNumber()
	{
		return this.nodeNumber;
	}
	
	public void setNodeNumber(int value)
	{
		this.nodeNumber = value;
	}

	public synchronized ArrayList<IncomingEdge> getIncomingEdgeList()
	{
		return this.incomingEdgeList;
	}
	
	public double getBiasValue()
	{
		return this.inputBiasValue;
	}
	
	public void setBiasValue(double bias)
	{
		this.inputBiasValue = bias;
	}
	
	public double getBiasWeight()
	{
		return this.inputBiasWeight;
	}
	
	public void setBiasWeight(double bias)
	{
		this.inputBiasWeight = bias;
	}
	
	public void setOutputValue(Double output)
	{
		this.outputValue = output;
	}
	
	public Double getOutputValue()
	{
		return this.outputValue;
	}
	


}
