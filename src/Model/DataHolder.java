package Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Map.Entry;
import java.util.Random;
import java.util.UUID;

import javax.swing.JFrame;

import Views.Observers;

import Controllers.Cycles;
import Evolvers.ActivationFunctionParent;
import Evolvers.Evolution;

public class DataHolder
{
	private NeuralNetwork fittestEver = null;
	public static final long timeStamp = new Date().getTime();
	private ArrayList<Observers> observers = new ArrayList<Observers>();
	private HashMap<String,Boolean> initialised = new HashMap<String,Boolean>();
	
	// 
	private HashMap<String,NeuralNetwork> linearPopulation = new HashMap<String,NeuralNetwork>();
	private String functionToApprox;
	
	private Double populationSize = 0.0;
	private Double crossoverRate = 0.0;
	private Double mutationRate = 0.0;
	private Double weightMinimum = 0.0;
	private Double weightMaximum = 0.0;
	Double LMS = 0.0;
	
	private Integer currentEvolutionNumber = 0;
	public int numberEvolutions = 0;
	
	private DecimalFormat decimalFormat = new DecimalFormat("#.####");
	
	public ArrayList<Double> currentEvolutionOverallFitnessValues = new ArrayList<Double>();
	ArrayList<Double> expectedOutputs = new ArrayList<Double>();
	ArrayList<Double> actualOutputs = new ArrayList<Double>();
	
	
	private Boolean evolveWeights = false;
	private Boolean evolveActivation = false;
	
	private ActivationFunctionParent activationFunction;
	
	public synchronized HashMap<String, Boolean> getInitialised() {
		return initialised;
	}

	public synchronized void setInitialised(HashMap<String, Boolean> initialised) {
		this.initialised = initialised;
	}
	
	public synchronized Double getLMS() {
		return LMS;
	}

	public synchronized void setLMS(Double lMS) {
		LMS = lMS;
	}

	public synchronized void addToExpectedOutputs(Double expected)
	{
		expectedOutputs.add(expected);
	}
	
	public synchronized void addToActualOutputs(Double actual)
	{
		actualOutputs.add(actual);
	}
	
	public synchronized ArrayList<Double> getExpectedOutputs() {
		return expectedOutputs;
	}

	public synchronized void setExpectedOutputs(ArrayList<Double> expectedOutputs) {
		this.expectedOutputs = expectedOutputs;
	}

	public synchronized ArrayList<Double> getActualOutputs() {
		return actualOutputs;
	}

	public synchronized void setActualOutputs(ArrayList<Double> actualOutputs) {
		this.actualOutputs = actualOutputs;
	}


	
	public synchronized String getFunctionToApprox() {
		return functionToApprox;
	}

	public synchronized void setFunctionToApprox(String functionToApprox) {
		this.functionToApprox = functionToApprox;
	}
	
	public synchronized ActivationFunctionParent getActivationFunction() {
		return activationFunction;
	}

	public synchronized void setActivationFunction(ActivationFunctionParent activationFunction) {
		this.activationFunction = activationFunction;
	}

	public synchronized Boolean getEvolveWeights() {
		return evolveWeights;
	}

	public synchronized void setEvolveWeights(Boolean evolveWeights) {
		this.evolveWeights = evolveWeights;
	}

	public synchronized Boolean getEvolveActivation() {
		return evolveActivation;
	}

	public synchronized void setEvolveActivation(Boolean evolveActivation) {
		this.evolveActivation = evolveActivation;
	}
	
	public synchronized void setFittestEver(NeuralNetwork ann)
	{
				this.fittestEver = ann;		
	}
	
	public synchronized NeuralNetwork getFittestEver()
	{
		return this.fittestEver;
	}

	public static final String NL = System.getProperty("line.separator");

	public DataHolder()
	{
		decimalFormat.setRoundingMode(RoundingMode.CEILING);
	}

	public void InitialiseInputsVariablesAndFirstPopulation()
	{
		// First of all read the inputs from the files given. 
		Inputs.Initialise();
		try 
		{
			Inputs.ReadInputs();
		} catch (IOException e) 
		{
			e.printStackTrace();
		}

		// First check how many inputs and read the values for them, creating appropriate input nodes
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try
		{
			//setNumberEvolutions(br);
			//setPopulationSize(br);
			//setCrossoverRate(br);
			//setMutationRate(br);
			//setMinimumWeight(br);
			//setMaximumWeight(br);

			br.close(); // Free up resources.
		}
		catch(IOException ioe)
		{
			Log.addToLog("IO exception reading line from standard in");
		}
		// Now create an ANN for each function to be approximated. 
		
		if(this.getActivationFunction().equals("null"))
		{
			
		}
		for(int x = 0 ; x < this.populationSize; x++)// population size was read in before.  
		{
			String uniqueID = UUID.randomUUID().toString();
			
			//Thread t = new Thread(new NeuralNetwork(uniqueID,weightMinimum,weightMaximum));
			linearPopulation.put(uniqueID,new NeuralNetwork(this,this.getActivationFunction(),uniqueID,weightMinimum,weightMaximum)); // Used for approximating the linear function. 
		}
	}
	
	
	
	public synchronized HashMap<String, NeuralNetwork> getCopyOfLinearPopulation()
	{
		HashMap<String,NeuralNetwork> temp = new HashMap<String,NeuralNetwork>();
		for(Entry<String,NeuralNetwork> entry : this.linearPopulation.entrySet())
		{
			temp.put(entry.getKey(),entry.getValue());
		}
		return temp;
	}

	public String setNumberEvolutions(Integer numberFromGUI/*,BufferedReader br*/) throws NumberFormatException, IOException
	{
		//System.out.println("Enter the number of evolutions you want to happen (1 to 1000) > ");
		numberEvolutions = numberFromGUI;//Integer.parseInt(br.readLine());
		if(numberEvolutions < 1 || numberEvolutions > 50000)
		{
			initialised.put("evol", false);
			Log.addToLog("just put some data into initialsed hashmap");
			Log.addToLog("Sorry please enter a number between 1 and 50,000");
			return "Sorry please enter a number between 1 and 50000";

			//setNumberEvolutions(numberFromGUI);
		}
		else
		{
			initialised.put("evol", true);
			Log.addToLog("Number of Evolutions will be "+decimalFormat.format(numberEvolutions));
			return "Number of Evolutions will be "+decimalFormat.format(numberEvolutions);
		}
	}

	public String setPopulationSize( Double numberFromGUI/*BufferedReader br*/) throws NumberFormatException, IOException
	{
		//System.out.println("Enter the starting population size > ");
		populationSize = numberFromGUI;//Double.parseDouble(br.readLine());
		if(populationSize < 1.0 || populationSize > 1000)
		{initialised.put("pops", false);
		Log.addToLog("Sorry please enter a number between 1 and 1000");
		return "Sorry please enter a number between 1 and 100";
		//setPopulationSize(numberFromGUI);
		}
		initialised.put("pops", true);
		Log.addToLog("Population size will be "+decimalFormat.format(populationSize));
		return "Population size will be "+decimalFormat.format(populationSize);
	}
	
	public Double getPopulationSize()
	{
		return this.populationSize;
	}
	
	public void clearLinearPopulation()
	{
		this.linearPopulation.clear();
	}

	public synchronized Double getCrossoverRate() {
		return crossoverRate;
	}

	public synchronized Double getWeightMinimum() {
		return weightMinimum;
	}

	public synchronized void setWeightMinimum(Double weightMinimum) {
		this.weightMinimum = weightMinimum;
	}

	public synchronized Double getWeightMaximum() {
		return weightMaximum;
	}

	public synchronized void setWeightMaximum(Double weightMaximum) {
		this.weightMaximum = weightMaximum;
	}

	public synchronized HashMap<String, NeuralNetwork> getLinearPopulation() {
		return linearPopulation;
	}

	public synchronized void setLinearPopulation(
			HashMap<String, NeuralNetwork> linearPopulation) {
		this.linearPopulation = linearPopulation;
	}
	
	public synchronized void addToLinearPopulation(String id, NeuralNetwork ann)
	{
		this.linearPopulation.put(id, ann);
	}

	public String setCrossoverRate(Double numberFromGUI) throws NumberFormatException, IOException
	{
		//System.out.println("Enter the crossover rate between 0.5 and 1 > ");
		crossoverRate = numberFromGUI;//Double.parseDouble(br.readLine());
		if(!(0.0005 <= crossoverRate && crossoverRate <= 1.0))
		{initialised.put("cros", false);
		Log.addToLog("Sorry please enter a crossover rate between 0.0005 and 1.0");
		return "Sorry please enter a crossover rate between 0.0005 and 1.0";
		//setCrossoverRate(br);
		}
		else
		{
			initialised.put("cros", true);
			Log.addToLog("Crossover rate will be "+decimalFormat.format(crossoverRate));
			return "Crossover rate will be "+decimalFormat.format(crossoverRate);
		}
		
	}

	public String setMutationRate(Double numberFromGUI) throws NumberFormatException, IOException
	{
		//System.out.println("Enter the mutation rate > ");
		mutationRate = numberFromGUI; //Double.parseDouble(br.readLine());
		if(!(0.001 <= mutationRate && mutationRate <= 1.0))
		{initialised.put("muta", false);
		Log.addToLog("Sorry please enter a mutation rate between 0.001 and 1.0");
		return "Sorry please enter a mutation rate between 0.001 and 1.0";
		//setMutationRate(br);
		}
		else
		{
			initialised.put("muta", true);
			Log.addToLog("mutation rate will be "+decimalFormat.format(mutationRate));
			return "mutation rate will be "+decimalFormat.format(mutationRate);
		}
		
	}

	public String setMinimumWeight(Double minWeight) throws NumberFormatException, IOException 
	{
		//System.out.println("Enter the weight interval minimum (no less than -5, no more than +5) > ");
		weightMinimum = minWeight; //Double.parseDouble(br.readLine());
		if(weightMinimum < -5.0 || weightMinimum > 5 || weightMinimum == 5)
		{initialised.put("wmin", false);
		Log.addToLog("Sorry please enter a min weight between -5 and +5 which is not positive 5");
		return "Sorry please enter a min weight between -5 and +5 which is not positive 5";
		//setMinimumWeight(br);
		}
		else
		{
			initialised.put("wmin", true);
			Log.addToLog("weight minimum is "+decimalFormat.format(weightMinimum));
			return "weight minimum is "+decimalFormat.format(weightMinimum);
		}
	}

	public String setMaximumWeight(Double maxWeight) throws NumberFormatException, IOException
	{
		//System.out.println("Enter the weight interval maximum (no less than -5, no more than +5) > ");
		weightMaximum = maxWeight; //Double.parseDouble(br.readLine());
		if(weightMaximum < weightMinimum)
		{initialised.put("wmax", false);
		Log.addToLog("That max weight was less than the minimum! Please try again");
		return "That max weight was less than the minimum! Please try again";
		//setMaximumWeight(br);
		}
		if(weightMaximum < -5.0 || weightMaximum > 5.0 || weightMaximum == null)
		{
			initialised.put("wmax", false);
			Log.addToLog("Sorry max weight should be between -5 and +5, please try again");
			return "Sorry max weight should be between -5 and +5, please try again";
			//setMaximumWeight(br);
		}
		else
		{
			initialised.put("wmax", true);
			Log.addToLog("weight maximum is "+decimalFormat.format(weightMaximum));
			return "weight maximum is "+decimalFormat.format(weightMaximum);
		}
	}

	public void mutate()
	{
		// If boolean is true for both weights and activation, evolve both
		if(this.getEvolveWeights() && this.getEvolveActivation())
		{
			// Now mutate the weights (and activation function if you want)
			for(NeuralNetwork ann : linearPopulation.values())
			{
				ann.mutateWeightsWithProbability(); // Mutate all the weights by incrementing or decrementing by a fraction. 
				ann.mutateActivationWithProbability();
				ann.mutateTopologyWithProbability();
			}
		}
		// If boolean is true for just weights, just evolve weights
		else if(this.getEvolveWeights() && !this.getEvolveActivation())
		{
			// Now mutate the weights
			for(NeuralNetwork ann : linearPopulation.values())
			{
				ann.mutateWeightsWithProbability(); // Mutate all the weights by incrementing or decrementing by a fraction. 
				ann.mutateTopologyWithProbability();
			}
		}
		
		// If boolean is true for just evolve activation, just do that. 
		else if(!this.getEvolveWeights() && this.getEvolveActivation())
		{
			// Now mutate the activation function 
			for(NeuralNetwork ann : linearPopulation.values())
			{
				ann.mutateActivationWithProbability();
				ann.mutateTopologyWithProbability();
			}
		}
		else{}
	}
	
	public void mutateOneANN(NeuralNetwork ann)
	{
		// If boolean is true for both weights and activation, evolve both
		if(this.getEvolveWeights() && this.getEvolveActivation())
		{
			// Now mutate the weights (and activation function if you want)
			
				ann.mutateWeightsWithProbability(); // Mutate all the weights by incrementing or decrementing by a fraction. 
				ann.mutateActivationWithProbability();
				ann.mutateTopologyWithProbability();
			
		}
		// If boolean is true for just weights, just evolve weights
		else if(this.getEvolveWeights() && !this.getEvolveActivation())
		{
			// Now mutate the weights
			
				ann.mutateWeightsWithProbability(); // Mutate all the weights by incrementing or decrementing by a fraction. 
				ann.mutateTopologyWithProbability();
			
		}
		
		// If boolean is true for just evolve activation, just do that. 
		else if(!this.getEvolveWeights() && this.getEvolveActivation())
		{
			// Now mutate the activation function 
			
				ann.mutateActivationWithProbability();
				ann.mutateTopologyWithProbability();
			
		}
		else{}
	}


	public Double getMutationRate() {
		return mutationRate;
	}

	public Integer getCurrentEvolutionNumber() {
		return currentEvolutionNumber;
	}

	public void setCurrentEvolutionNumber(Integer currentEvolutionNumber) {
		this.currentEvolutionNumber = currentEvolutionNumber;
	}

	public synchronized void addToCurrentEvolutionOverallFitnessValues(
			Double summer) {
		this.currentEvolutionOverallFitnessValues.add(summer);
	}

	public synchronized String getOverallFitnessValuesOneLine()
	{
		String returner = NL+"Evolution "+this.currentEvolutionNumber+": ";
		for(Double fitness : this.currentEvolutionOverallFitnessValues)
		{
			returner += decimalFormat.format(fitness).toString() + ", ";
		}
		return returner;
	}
	

	public void updateObservers()
	{
		for(Observers observer : this.observers)
		{
			observer.update();
		}
	}

	public void addObserver(Observers view)
	{
		this.observers.add(view);
	}



}
