package Model;
import java.text.SimpleDateFormat;
import java.util.*;

public class Log {
	//                   timestamp,message
	private static String log = "";
	// Integer is evolution number, arraylist holds fitness values for that evolution... 
	private static HashMap<Integer,ArrayList<Double>> fitnessLog = new HashMap<Integer,ArrayList<Double>>();
	public static final String NL = System.getProperty("line.separator");
	
	public Log(){}
	
	public synchronized static void addToLog(String toAdd)
	{
		log+=NL+toAdd;
	}
	
	public static void clearLog()
	{
		log = "";
	}
	
	// Synchronized keyword because multiple threads are accessing this. 
	public synchronized static void addJustOverallFitness(Integer evolutionNumber, Double summer)
	{
		if(fitnessLog.get(evolutionNumber) == null) // If first time round add a new arraylist
		{
			fitnessLog.put(evolutionNumber,new ArrayList<Double>());
			fitnessLog.get(evolutionNumber).add(summer);
		}
		
		else // otherwise add to the arraylist. 
		{
			fitnessLog.get(evolutionNumber).add(summer);
		}
	}
	
	// Synchronized keyword because multiple threads are accessing this. 
	public synchronized static String getOverallFitnesses()
	{
		String returner = "";
		for(Integer i : fitnessLog.keySet())
		{
			returner += "Evolution number: "+i.toString() + "\n";
			for(Double s : fitnessLog.get(i))
			{
				returner += "\t"+s.toString()+"\n";
			}
		}
		
		return returner;
	}
	
	public synchronized static String getOverallFitnessesOneLine()
	{
		String returner = "";
		for(Integer i : fitnessLog.keySet())
		{
			returner += NL+"Evolution number: "+i.toString()+": ";
			for(Double s : fitnessLog.get(i))
			{
				returner += "- "+s.toString()+" ";
			}
		}
		
		return returner;
	}
	
	public static String getLog()
	{
		return log;
	}

}
