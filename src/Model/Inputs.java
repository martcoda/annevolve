package Model;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Inputs {
	
	private static HashMap<String,ArrayList<Double[]>> functions = new HashMap<String,ArrayList<Double[]>>();
	
	private ArrayList<Double[]> linear;
	private ArrayList<Double[]> cubic;
	private ArrayList<Double[]> sine;
	private ArrayList<Double[]> tanh;
	private ArrayList<Double[]> complex;
	private ArrayList<Double[]> xor;
	
	public static Boolean Initialise()
	{
		// Set the filenames to read from as inputs. 
		Inputs.functions.put("1in_linear.txt", null);
		Inputs.functions.put("1in_cubic.txt", null);
		Inputs.functions.put("1in_sine.txt", null);
		Inputs.functions.put("1in_tanh.txt", null);
		Inputs.functions.put("2in_complex.txt", null);
		Inputs.functions.put("2in_xor.txt", null);
		
		return true;
	}
	
	public static void ReadInputs() throws IOException
	{
		for(Entry<String,ArrayList<Double[]>> e : Inputs.functions.entrySet())
		{
			if(e.getKey().equals("2in_complex.txt") || e.getKey().equals("2in_xor.txt"))
			{
				functions.put(e.getKey(),ReadAndWrite.readAndStoreInputs(e.getKey(), 3));
			}
			else
			{
				functions.put(e.getKey(),ReadAndWrite.readAndStoreInputs(e.getKey(), 2));
			}
		}
	}
	
	public synchronized static HashMap<String,ArrayList<Double[]>> getFunctionsData()
	{
		return Inputs.functions;
	}
}
