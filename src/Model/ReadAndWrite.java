package Model;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;


public class ReadAndWrite {

	
	public ReadAndWrite()
	{
		this.getClass();
	}
		
	public static ArrayList<Double[]> readAndStoreInputs(String fileName, int numberFields) throws IOException {
		
		System.out.println("filename is "+fileName);
		//Scanner gives you useful methods like .next(), readLine()
		Scanner sc = new Scanner(ReadAndWrite.class.getResourceAsStream(fileName)); // Had this.getClass() but changed to ReadAndWrite.class because static wouldn't allow "this" keyword. 
		String line = "";
		ArrayList<Double[]> fieldArrays = new ArrayList<Double[]>();
		
		while(sc.hasNext())
		{//while more lines to read in file
			line = sc.nextLine();//read line and put into line String
			
			Scanner fielder = new Scanner(line);//.useDelimiter(" ");//in text file, tokens are separated by spaces by default so don't need to say delimiter
			Double[] tempFieldArray = new Double[numberFields];
			for(int x = 0; x < numberFields; x++)
			{
				if(fielder.hasNext())
				{//safer to check that there is another token to be read
					String tempString = fielder.next(); //get first field
					tempString = tempString.trim(); //delete white spaces
					//System.out.println("current field number is "+x+" and field value is "+tempString);
					// Put value into the array
					tempFieldArray[x] = Double.parseDouble(tempString);
				}
			}
			// Add the field array to the ArrayList to be returned. 
			fieldArrays.add(tempFieldArray);
		}
		return fieldArrays;
	}
	
	public static void writeToFile(String towrite)
	{
        //writes string to file
        try
        {    
        
            File myfile = new File("log-"+DataHolder.timeStamp+".txt");
            FileWriter writeReport = new FileWriter(myfile,true);//true means append
            writeReport.write(towrite);
            writeReport.flush();
            writeReport.close();
            
            StringBuffer writtenMessage = new StringBuffer();
            writtenMessage.append("Report written to ").append(myfile.getAbsolutePath());
            String wMess = writtenMessage.toString();
            //JOptionPane.showMessageDialog(null, wMess);
            System.out.println(wMess);
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
            System.exit(1);
            JOptionPane.showMessageDialog(null, "Failed to write to log....");
        }
	}
}
