package Controllers;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.util.Date;

import Evolvers.Evolution;
import Model.DataHolder;
import Model.Log;
import Views.GUI;
import Views.GraphLMS;
import Views.Grapher;
import Views.GrapherFittest;

public class Controller {

	private DataHolder dataholder;
	private GUI gui;

	public Controller(DataHolder dataholder,GUI gui)
	{
		this.dataholder = dataholder;
		this.gui = gui;
		gui.addGleanButtonListener(new GleanUserEnteredVars(this));
		gui.addDefaultsButtonListener(new DefaultsButtonListener(this));
	}
	
	private void Go()
	{
		Grapher grapher = new Grapher("graph",dataholder);
		grapher.pack();
	    //RefineryUtilities.centerFrameOnScreen(grapher);
	    grapher.setVisible(true);
	    
	    GrapherFittest grapherFittest = new GrapherFittest("graph of fittest",dataholder);
	    grapherFittest.pack();
	    //RefineryUtilities.centerFrameOnScreen(grapherFittest);
	    grapherFittest.setVisible(true);
	    
	    GraphLMS graphLMS = new GraphLMS("graph of LMS",dataholder);
	    graphLMS.pack();
	    //RefineryUtilities.centerFrameOnScreen(graphLMS);
	    graphLMS.setVisible(true);
		
		System.out.println("working");
		Log.addToLog("working");
		
		gui.disableSubmitButton();
		
		dataholder.InitialiseInputsVariablesAndFirstPopulation();

		// Do many evolutions
		this.doXEvolutions(dataholder.numberEvolutions, dataholder);

		//System.out.println(Log.getOverallFitnesses());

		/*System.out.println("Program finished");
		Log.addToLog("Program finished");
		gui.messages.setText(gui.messages.getText()+dataholder.NL+"PROGRAM FINISHED");*/
	}
	
	public void doXEvolutions(int x, DataHolder dataholder)
	{
		Evolution evo = new Evolution(dataholder);
		// Do evolutions in a separate thread to free up the GUI for repainting updates. 
		Thread t = new Thread(new Cycles(dataholder,x, evo));
		t.start();
	}
	
	
	class DefaultsButtonListener implements ActionListener
	{
		private Controller controller;
		public DefaultsButtonListener(Controller controller)
		{
			this.controller = controller;
		}
		
		public void actionPerformed(ActionEvent e) 
		{ 
			gui.fillInDefaultValues(e);
		}
	}
	//this class is for actioning events triggered in the GUI, e.g. pressing buttons
	class GleanUserEnteredVars implements ActionListener
	{
		private Controller controller;
		public GleanUserEnteredVars(Controller controller)
		{
			this.controller = controller;
		}
		public void actionPerformed(ActionEvent e) 
		{ 
			gui.sendInitialVarsToModel(e);

			Boolean go = true;
			if(controller.dataholder.getInitialised().size() == 6)
			{
				for(Boolean b : dataholder.getInitialised().values())
				{
					if(b == false)
					{
						go = false;
					}
				}
				// If go is still true then initialised must be true for all user-entered vars
				if(go)
				{
				controller.Go();
				}
			}
			else
			{
				System.out.println("Some inputs haven't been entered yet, so wait for them");
				Log.addToLog("Some inputs haven't been entered yet, so wait for them");
			}

		}
	}
}

