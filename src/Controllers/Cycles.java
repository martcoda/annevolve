package Controllers;
import java.util.ArrayList;
import java.util.Map.Entry;

import Evolvers.Evolution;
import Model.DataHolder;
import Model.IncomingEdge;
import Model.Log;
import Model.NeuralNetwork;
import Model.Node;
import Model.ReadAndWrite;

public class Cycles implements Runnable {

	private DataHolder dataholder;
	private int numberEvolutions;
	private Evolution evolution;
	public Cycles(DataHolder dataholder, int x, Evolution evolution)
	{
		this.dataholder = dataholder;
		this.numberEvolutions = x;
		this.evolution = evolution;
	}
	
	public void run()
	{
		Log.addToLog("Starting evolution cycles");
		// Do x more evolutions!!! 
		for(int y = 0 ; y < this.numberEvolutions; y++) // x evolutions. 
		{
			Log.addToLog("Starting evolution cycle "+y);
			dataholder.currentEvolutionOverallFitnessValues.clear();
			dataholder.getActualOutputs().clear();
			
			dataholder.setCurrentEvolutionNumber(y);
			Boolean currentEvolutionCycleFinished = false;
			ArrayList<Thread> tempThreadList = new ArrayList<Thread>();
			
			for(NeuralNetwork ann : dataholder.getLinearPopulation().values())
			{
				Thread temp = new Thread(ann);
				tempThreadList.add(temp);
				temp.start(); // Start the ANN going
			}
			// Wait for current evolution cycle to finish. 
			while(!currentEvolutionCycleFinished)
			{
				try 
				{
					Boolean anyAlive = false;
					for(Thread thread : tempThreadList)
					{
						if(thread.isAlive()) // Check if the ANN still going
						{
							anyAlive = true;
							Log.addToLog("ANN thread is still running");
						}
					}
					if(!anyAlive)
					{
						currentEvolutionCycleFinished = true;
					}
					Thread.sleep(50); // Wait before checking again. 
				} catch (InterruptedException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}// Wait for ANNs to finish processing. 
			}
			
			Boolean fitnessSet = false;
			while(!fitnessSet)
			{
				fitnessSet = true;
				// Check that all fitness values have been set. 
				for(NeuralNetwork ann : dataholder.getCopyOfLinearPopulation().values())
				{
					if(ann.getOverallFitness() == null)
					{
						System.out.println(ann.populationMemberID+" has not yet set the fitness value...");
						Log.addToLog(ann.populationMemberID+" has not yet set the fitness value...");
						fitnessSet = false;
					}
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				} // Wait for all fitness values to be set. 
			}
			
			//System.out.println(Log.getOverallFitnesses());
			Log.addToLog("Evolution cycle "+y+" has all ANN threads dead, moving onto selection, breeding, mutation");
			
			this.evolution.setFinishedBreeding(false);
			this.evolution.selectParentsAndBreed(); // In this will also add to actual and expected outputs arrays in dataholder (used to draw the graphs)
			
			while(!evolution.getFinishedBreeding())
			{
				try {
					Thread.sleep(20);
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			Log.addToLog("Ok finished breeding now, moving onto mutation");
			// Mutate each ANN in the current population
			//actually don't need to do that as doing mutation in the breeding cycle
			//dataholder.mutate();
			
			// Wait until mutation has finished before moving on. 
			/*Boolean mutationFinished = false;
			while(!mutationFinished)
			{
				Boolean litmus = true;
				for(NeuralNetwork ann : dataholder.getCopyOfLinearPopulation().values())
				{
					if(!ann.getFinishedMutating())
					{
						litmus = false;
					}
				}
				if(litmus)
				{
					mutationFinished = true;
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}*/
			
			Log.addToLog("Evolution cycle "+y+" finished!!!");
			// Finally print log to file, then clear log. 
			ReadAndWrite.writeToFile(Log.getLog());
			Log.clearLog();
			
			dataholder.updateObservers(); // Should go to parent Observers class. 
			
			//System.out.println(Log.getOverallFitnesses());
			
		}//end of for-loop cycling through number of evolutions
		
		Log.addToLog("Fittest ever has fitness of "+dataholder.getFittestEver().getOverallFitness()+" and breakdown is: ");
		for(Double d : dataholder.getFittestEver().getDifferenceBetweenEandO())
		{
			Log.addToLog("Difference between expected and actual for fittest ever is "+d.toString());
		}
		
		// Now to print all the weight/activation details and topology which resulted in the high score
		Log.addToLog(dataholder.NL+"---------------------------------------------------------");
		Log.addToLog(dataholder.NL+"FITTEST EVER ANN HAD ID "+dataholder.getFittestEver().populationMemberID
				+" and fitness "+dataholder.getFittestEver().getOverallFitness()+dataholder.NL+" and LMS "
				+dataholder.getFittestEver().calculateLMS()+" and was from evolution number "
				+dataholder.getFittestEver().getEvolutionCycle());
		
		Log.addToLog("It was approximating function "+dataholder.getFunctionToApprox());
		Log.addToLog(dataholder.NL+"First layer has "+dataholder.getFittestEver().getFirstLayerNodes().size()+" nodes");
		for(int x = 0; x < dataholder.getFittestEver().getFirstLayerNodes().size(); x++)
		{
			Log.addToLog("First layer node "+x+" has bias weight: "+dataholder.getFittestEver().getFirstLayerNodes().get(x).getBiasWeight()+", and bias value of "+dataholder.getFittestEver().getFirstLayerNodes().get(x).getBiasValue()+" and activation of "+dataholder.getFittestEver().getFirstLayerNodes().get(x).getActivationFunction().toString()+" and has incoming edges:");
			for(IncomingEdge entry : dataholder.getFittestEver().getFirstLayerNodes().get(x).getIncomingEdgeList())
			{
				Log.addToLog("\tEdge -  weight value: "+entry.weightValue);
			}
		}
		
		Log.addToLog(dataholder.NL+"There are "+dataholder.getFittestEver().getHiddenLayers().size()+" hidden layers");
		for(int z = 0; z < dataholder.getFittestEver().getHiddenLayers().size(); z++)
		{
			Log.addToLog("Hidden layer "+z+" has the following nodes: ");
			for(int x = 0; x < dataholder.getFittestEver().getHiddenLayers().get(z).size(); x++)
			{
				Log.addToLog("\tHidden layer node "+x+" has bias weight: "+dataholder.getFittestEver().getHiddenLayers().get(z).get(x).getBiasWeight()+", and bias value of "+dataholder.getFittestEver().getHiddenLayers().get(z).get(x).getBiasValue()+" and activation function "+dataholder.getFittestEver().getHiddenLayers().get(z).get(x).getActivationFunction().toString()+" and incoming edges:");
				
				for(IncomingEdge entry : dataholder.getFittestEver().getHiddenLayers().get(z).get(x).getIncomingEdgeList())
				{
					Log.addToLog("\t\tEdge weight value: "+entry.weightValue);
				}
			}
		}
		
		Log.addToLog(dataholder.NL+"Output layer node has bias weight: "+dataholder.getFittestEver().getOutputNode().getBiasWeight()+", and bias value of "+dataholder.getFittestEver().getOutputNode().getBiasValue()+" and activation function "+dataholder.getFittestEver().getOutputNode().getActivationFunction().toString()+" and incoming edges:");
		for(IncomingEdge entry : dataholder.getFittestEver().getOutputNode().getIncomingEdgeList())
		{
			Log.addToLog("\tEdge weight value: "+entry.weightValue);
		}
		
		Log.addToLog("Program finished");
		ReadAndWrite.writeToFile(Log.getLog());
		Log.clearLog();
	}
}
