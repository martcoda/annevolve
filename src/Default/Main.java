package Default;

import org.jfree.ui.RefineryUtilities;

import Controllers.Controller;
import Model.DataHolder;
import Views.GUI;
import Views.GraphLMS;
import Views.Grapher;
import Views.GrapherFittest;


public class Main {
	
	public static void main(String[] args)
	{
		DataHolder dataholder = new DataHolder();
		
		GUI gui = new GUI(dataholder);
		
		Controller controller = new Controller(dataholder,gui);
	}
}
