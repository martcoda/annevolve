
The jars in lib folder are not my creation and are licensed under the LGPL. 

The other files are my creations and should also be considered as licensed 
under the LGPL. 

For more details of the LGPL see https://www.gnu.org/copyleft/lesser.html 

A copy is kept in the root of this project. 
